package com.przyjaznydamianek.dao;

import android.database.sqlite.SQLiteDatabase;

import com.przyjaznydamianek.dto.TerminarzDto;

/**
 * Created by Chris on 10/24/2014.
 */
public class TerminarzDao extends AbstractDao<TerminarzDto> {

    public TerminarzDao(SQLiteDatabase db){
        super(db);
    }

    @Override
    public void create(TerminarzDto object) {
        //TODO
    }

    @Override
    public TerminarzDto read(TerminarzDto object) {
        return null;
    }

    @Override
    public void update(TerminarzDto object) {
        try {
            db.update(object.getTable().getTableName(), object.getContentValues(), object.getCOLUMN_ID() + "=" + object.getID(), null);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void delete(TerminarzDto objBaseDto) {
        try {
            db.delete(objBaseDto.getTable().getTableName(), objBaseDto.getCOLUMN_ID() + "=" + objBaseDto.getID(), null);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
