package com.przyjaznydamianek.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.DbHelper.PKGen;
import com.przyjaznydamianek.models.Activity;
import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianek.dto.PlanDto;
import com.przyjaznydamianek.sqlCreate.AK_PL;
import com.przyjaznydamianek.sqlCreate.CZ_AK;
import com.przyjaznydamianek.utils.BusinessLogic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chris on 10/24/2014.
 */
public class PlanDao extends AbstractDao<PlanDto> {


    public PlanDao(SQLiteDatabase db){
        super(db);
    }

    private ContentValues createAKPLObject(String id_ak, String id_pl, String flag, int pos, String status){
        ContentValues values = new ContentValues();
        String id = PKGen.GenPK();
        values.put(AK_PL.ID,id);
        values.put(AK_PL.ID_AK,id_ak);
        values.put(AK_PL.ID_PL,id_pl);
        values.put(AK_PL.TYP_AK,flag);
        values.put(AK_PL.POZ_AK,pos);
        if(status == null){
            status = Activity.ActivityStatus.NEW.toString();
        }
        values.put(AK_PL.STATUS_AK, status);
        return values;
    }

    private void addOrUpdateActivitiesFromPlan(String planId, List<Activity> aktywnosci, List<Activity> aktywnosciGA , List<Activity> aktywnosciPrzerwy){
        String flag = null;
        for(int i=0; i<aktywnosci.size(); i++){
            if(aktywnosci.get(i).getTypeFlag()!= null && aktywnosci.get(i).getTypeFlag().equals(Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString())){
                flag = Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString();
            } else {
                if(aktywnosci.get(i).getTypeFlag()!= null && aktywnosci.get(i).getTypeFlag().equals(Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString())){
                    flag = Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString();
                } else {
                    flag = Activity.TypeFlag.ACTIVITY.toString();
                }
            }
            db.insert(AK_PL.TABLE_NAME, null, createAKPLObject(aktywnosci.get(i).getId(),planId,flag,i,aktywnosci.get(i).getStatus()));
        }
        for(int i=0; i<aktywnosciGA.size(); i++){
            db.insert(AK_PL.TABLE_NAME, null, createAKPLObject(aktywnosciGA.get(i).getId(),planId,Activity.TypeFlag.ACTIVITY_GALLERY.toString(),i,aktywnosciGA.get(i).getStatus()));
        }
        for(int i=0; i<aktywnosciPrzerwy.size(); i++){
            db.insert(AK_PL.TABLE_NAME, null, createAKPLObject(aktywnosciPrzerwy.get(i).getId(),planId,Activity.TypeFlag.BREAK.toString(),i,aktywnosciPrzerwy.get(i).getStatus()));
        }
    }

    public List<Plan> getPlanByTitle(String title){
        List<Plan> plany = new ArrayList<Plan>();

        Cursor cursor = null;
        try {
            cursor=db.query(com.przyjaznydamianek.sqlCreate.Plan.TABLE_NAME,
              com.przyjaznydamianek.sqlCreate.Plan.PLAN_ALL_COLUMNS, "(" + com.przyjaznydamianek.sqlCreate.Plan.NAZWA+ " LIKE ?)and(" + com.przyjaznydamianek.sqlCreate.Plan.ID +"!='"+ BusinessLogic.SYSTEM_AKTUALNY_PLAN_ID +"')", new String[] {"%"+ title+ "%" } ,null, null, com.przyjaznydamianek.sqlCreate.Plan.NAZWA + " ASC", null);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Plan plan = cursorToPlan(cursor);
            plany.add(plan);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return plany;
    }

    @Override
    public void create(PlanDto object) {
        try {
            object.createMode = true;
            String id = PKGen.GenPK();
            if(object.getContentValues().get(com.przyjaznydamianek.sqlCreate.Plan.ID)==null){
                object.getContentValues().put(com.przyjaznydamianek.sqlCreate.Plan.ID,id);
                object.getPlan().setId(id);
            }
            long val=db.insert(object.getTable().getTableName(), null, object.getContentValues());

            this.addOrUpdateActivitiesFromPlan(id, object.getPlan().getActivities(), object.getPlan().getAktywnosciGaleriiAktywnosci(), object.getPlan().getAktywnosciPrzerwy());

        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    @Override
    public PlanDto read(PlanDto object) {

        Cursor cursor = null;
        try {
            cursor = db.query(object.getTable().getTableName(), object.getColumnsToRead(), object.getSelectionString(), object.getSelectionArgs(), object.groupBy(), object.having(),object.orderBy());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }


        Plan plan = new Plan();
        cursor.moveToFirst();
        //TODO
        PlanDto planDto = new PlanDto();
        planDto.setPlan(plan);
        cursor.close();
        return null;
    }

    @Override
    public void update(PlanDto object) {
        try {
            db.update(object.getTable().getTableName(), object.getContentValues(), object.getCOLUMN_ID() + "='" + object.getID()+"'", null);
            db.delete(AK_PL.TABLE_NAME, AK_PL.ID_PL + " = '" + object.getPlan().getId()+"'" , null);
            this.addOrUpdateActivitiesFromPlan(object.getPlan().getId(), object.getPlan().getActivities(), object.getPlan().getAktywnosciGaleriiAktywnosci(), object.getPlan().getAktywnosciPrzerwy());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void delete(PlanDto objBaseDto) {
        try {
            db.delete(AK_PL.TABLE_NAME, AK_PL.ID_PL + " = '" + objBaseDto.getPlan().getId()+"'" , null);
            db.delete(objBaseDto.getTable().getTableName(), objBaseDto.getCOLUMN_ID() + "='" + objBaseDto.getID()+"'" , null);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public Plan getAktualnyPlan(String systemAktualnyPlanId) {
        Cursor cursor = null;
        try {
            cursor=db.query(com.przyjaznydamianek.sqlCreate.Plan.TABLE_NAME,
               com.przyjaznydamianek.sqlCreate.Plan.PLAN_ALL_COLUMNS, com.przyjaznydamianek.sqlCreate.Plan.ID+ " = '" + systemAktualnyPlanId+"'" , null ,null, null, null, null);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        cursor.moveToFirst();
        Plan plan = cursorToPlan(cursor);
        cursor.close();
        return plan;
    }

    private Plan cursorToPlan(Cursor cursor){
        ActivityDao adao = new ActivityDao(MySQLiteHelper.getDb());
        Plan p = new Plan();
        String planId = cursor.getString(cursor.getColumnIndex(com.przyjaznydamianek.sqlCreate.Plan.ID));
        p.setId(planId);
        p.setTitle(cursor.getString(cursor.getColumnIndex(com.przyjaznydamianek.sqlCreate.Plan.NAZWA)));
        p.setActivities(adao.getActivitiesAndTempAGFromPlan(planId));
        p.setAktywnosciPrzerwy(adao.getBreaksFromPlan(planId,0));
        p.setAktywnosciGaleriiAktywnosci(adao.getActivityGalleryFromPlan(planId, 0));
        return p;
    }

}
