package com.przyjaznydamianek.interfaces;

/**
 * Created by Chris on 10/24/2014.
 */
public interface DaoSql<T> {

    void create(T object);
    T read(T object);
    void update(T object);
    void delete(T objBaseDto);

}
