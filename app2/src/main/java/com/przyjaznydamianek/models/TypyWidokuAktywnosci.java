package com.przyjaznydamianek.models;

/**
 * Created by chris on 28.12.14.
 */
public enum TypyWidokuAktywnosci {

    big("duzy"),
    medium("sredni"),
    small("maly");

    private final String name;

    private TypyWidokuAktywnosci(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public static TypyWidokuAktywnosci getEnum(String s){
        if(s.equals(big.toString()))
                return big;
        if(s.equals(medium.toString()))
            return medium;
        if(s.equals(small.toString()))
            return small;

        return null;
    }

    public String toString() {
        return name;
    }

}
