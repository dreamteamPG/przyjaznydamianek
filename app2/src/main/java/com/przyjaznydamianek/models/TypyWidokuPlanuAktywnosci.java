package com.przyjaznydamianek.models;

/**
 * Created by chris on 12.01.15.
 */
public enum  TypyWidokuPlanuAktywnosci {

    list("lista"),
    slide("slajd");

    private final String name;

    private TypyWidokuPlanuAktywnosci(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public static TypyWidokuPlanuAktywnosci getEnum(String s){
        if(s.equals(list.toString()))
            return list;
        if(s.equals(slide.toString()))
            return slide;


        return null;
    }

    public String toString() {
        return name;
    }

}
