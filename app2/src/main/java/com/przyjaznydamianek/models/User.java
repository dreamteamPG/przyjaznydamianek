package com.przyjaznydamianek.models;

/**
 * Created by Chris on 10/25/2014.
 */
public class User extends BaseModel {
    private String id;
    private String name;
    private String surname;


    private UserPreferences preferences;

    public UserPreferences getPreferences() {
        return preferences;
    }

    public void setPreferences(UserPreferences preferences) {
        this.preferences = preferences;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getId() {

        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
