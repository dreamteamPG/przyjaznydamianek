package com.przyjaznydamianek.models;

/**
 * Created by chris on 28.12.14.
 */
public enum TypyWidokuCzynnosci {

    advanced("zaawansowany"),
    basic("podstawowy");


    private final String name;

    private TypyWidokuCzynnosci(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public static TypyWidokuCzynnosci getEnum(String s){
        if(s.equals(advanced.toString()))
            return advanced;
        if(s.equals(basic.toString()))
            return basic;


        return null;
    }

    public String toString() {
        return name;
    }
}
