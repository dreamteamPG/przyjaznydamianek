package com.przyjaznydamianek.models;

/**
 * Created by Chris on 10/26/2014.
 */
public class Slide extends BaseModel {
    private String id;
    private long settingsId;
    private String text;
    private int status;
    private String imagePath;
    private String audioPath;
    private int time;
    private String idActivity;
    private int position;

    public static enum SlideStatus {

        NEW(0), STARTED(1), FINISHED(2), CHOSEN(3);

        private int value;

         SlideStatus(int v){
            value=v;
        }

        public int getValue(){return value;}
    } ;

    public String getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(String idActivity) {
        this.idActivity = idActivity;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private boolean defaultSettings;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String title) {
        this.text = title;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public int getTime() {
        return time;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setSettingsId(long id) {
        defaultSettings = id == 0 ? true : false;
        settingsId=id;
    }
    public long getSettingsId() {
        return settingsId;
    }

    public boolean isDefaultSettings() {
        return defaultSettings;
    }
}
