package com.przyjaznydamianek.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 *
 */
public class Plan extends BaseModel {
    private String id;
    private String title;
    private List<Activity> activities;
    private List<com.przyjaznydamianek.models.Activity> aktywnosciPrzerwy;
    private List<com.przyjaznydamianek.models.Activity> aktywnosciGaleriiAktywnosci;

    public Plan(){
        this.activities = new ArrayList<Activity>();
        this.aktywnosciPrzerwy = new ArrayList<Activity>();
        this.aktywnosciGaleriiAktywnosci = new ArrayList<Activity>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public List<Activity> getAktywnosciPrzerwy() {
        return aktywnosciPrzerwy;
    }

    public void setAktywnosciPrzerwy(List<Activity> aktywnosciPrzerwy) {
        this.aktywnosciPrzerwy = aktywnosciPrzerwy;
    }

    public List<Activity> getAktywnosciGaleriiAktywnosci() {
        return aktywnosciGaleriiAktywnosci;
    }

    public void setAktywnosciGaleriiAktywnosci(List<Activity> aktywnosciGaleriiAktywnosci) {
        this.aktywnosciGaleriiAktywnosci = aktywnosciGaleriiAktywnosci;
    }

    @Override
    public String toString() {
        return title;
    }


}
