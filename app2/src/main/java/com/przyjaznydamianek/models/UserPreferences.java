package com.przyjaznydamianek.models;

/**
 * Created by chris on 28.12.14.
 */
public class UserPreferences extends BaseModel {


    private String id;
    private TypyWidokuCzynnosci typWidokuCzynnosci;
    private TypyWidokuAktywnosci typyWidokuAktywnosci;
    private TypyWidokuPlanuAktywnosci typWidokuPlanuAtywnosci;

    private String timerSoundPath;

    public TypyWidokuPlanuAktywnosci getTypWidokuPlanuAtywnosci() {
        return typWidokuPlanuAtywnosci;
    }

    public void setTypWidokuPlanuAtywnosci(TypyWidokuPlanuAktywnosci typWidokuPlanuAtywnosci) {
        this.typWidokuPlanuAtywnosci = typWidokuPlanuAtywnosci;
    }

    public TypyWidokuCzynnosci getTypWidokuCzynnosci() {
        return typWidokuCzynnosci;
    }

    public void setTypWidokuCzynnosci(TypyWidokuCzynnosci typWidokuCzynnosci) {
        this.typWidokuCzynnosci = typWidokuCzynnosci;
    }

    public TypyWidokuAktywnosci getTypyWidokuAktywnosci() {
        return typyWidokuAktywnosci;
    }

    public void setTypyWidokuAktywnosci(TypyWidokuAktywnosci typyWidokuAktywnosci) {
        this.typyWidokuAktywnosci = typyWidokuAktywnosci;
    }

    public String getId() {

        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimerSoundPath() {
        return timerSoundPath;
    }

    public void setTimerSoundPath(String timerSoundPath) {
        this.timerSoundPath = timerSoundPath;
    }
}
