package com.przyjaznydamianek.sqlCreate;

/**
 * Created by chris on 28.12.14.
 */
public class UST_USER extends BaseSQL{

    public static final String TABLE_NAME = "UST_USER";
    public static final String ID = "ID";
    public static final String ID_USER = "ID_USER";
    public static final String ID_USER_PREFERENCES = "ID_USTAWIENIA";


    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID_USER,TEXT_FIELD,REFERENCES(Uzytkownik.TABLE_NAME,Uzytkownik.ID))
                .addField(ID_USER_PREFERENCES,TEXT_FIELD,REFERENCES(UstawieniaUzytkownika.TABLE_NAME,UstawieniaUzytkownika.ID))
                .finishCreateSql();
    }
}
