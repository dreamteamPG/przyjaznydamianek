package com.przyjaznydamianek.sqlCreate;

/**
 *
 */
public final class Plan extends BaseSQL{

    public static final String TABLE_NAME="PLAN";
    public static final String ID = "ID";
    public static final String NAZWA = "NAZWA";

    public static final String[] PLAN_ALL_COLUMNS = {ID, NAZWA};

    public String getTableName(){
        return TABLE_NAME;
    }

    @Override
    public String createSql() {

                return createTable(TABLE_NAME)
                        .addField(ID, TEXT_FIELD, PRIMARY_KEY)
                        .addField(NAZWA, TEXT_FIELD, NOT_NULL)
                        .finishCreateSql();
    }
}
