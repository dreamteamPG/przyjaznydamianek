package com.przyjaznydamianek.sqlCreate;

/**
 * Created by Chris on 10/21/2014.
 */
public final class Czynnosc extends BaseSQL{

    public static final String TABLE_NAME="CZYNNOSC";
    public static final String ID = "ID";
    public static final String TEXT = "TEKST";
    public static final String IMAGE = "OBRAZEK";
    public static final String AUDIO = "AUDIO";
    public static final String TIMER = "TIMER";
    public static final String STATUS ="STATUS";

    public static final String[] COLUMN_SLIDE_ALL_COLUMNS = {ID, TEXT, AUDIO, TIMER, IMAGE, STATUS};




    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID,TEXT_FIELD,PRIMARY_KEY)
                .addField(TEXT,TEXT_FIELD)
                .addField(AUDIO,TEXT_FIELD)
                .addField(TIMER,INTEGER_FIELD)
                .addField(IMAGE,TEXT_FIELD)
                .addField(STATUS,INTEGER_FIELD)
                .finishCreateSql();

    }


    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
