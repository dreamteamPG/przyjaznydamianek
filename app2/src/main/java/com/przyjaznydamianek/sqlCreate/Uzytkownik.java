package com.przyjaznydamianek.sqlCreate;

/**
 * Created by Chris on 10/21/2014.
 */
public final class Uzytkownik extends BaseSQL {

    public static final String TABLE_NAME="UZYTKOWNIK";
    public static final String ID = "ID";
    public static final String NAME="IMIE";
    public static final String SURNAME="NAZWISKO";

    public static final String[] ALL_COLUMNS={ID,NAME,SURNAME};

    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID,TEXT_FIELD,PRIMARY_KEY)
                .addField(NAME,TEXT_FIELD)
                .addField(SURNAME, TEXT_FIELD)
                .finishCreateSql();
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
