package com.przyjaznydamianek.sqlCreate;

/**
 * Created by Chris on 10/21/2014.
 */
public final class CZ_AK extends BaseSQL{

    public static final String TABLE_NAME="CZ_AK";
    public static final String ID = "ID";
    public static final String ID_CZ = "ID_CZ";
    public static final String ID_AK = "ID_AK";
    public static final String POZ = "POZ";

    public static final String[] ALL_COLUMNS = {ID,ID_CZ,ID_AK,POZ};

    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID,TEXT_FIELD,PRIMARY_KEY)
                .addField(ID_CZ,TEXT_FIELD,REFERENCES(Czynnosc.TABLE_NAME,Czynnosc.ID))
                .addField(ID_AK,TEXT_FIELD,REFERENCES(Aktywnosc.TABLE_NAME,Aktywnosc.ID))
                .addField(POZ,INTEGER_FIELD)
                .finishCreateSql();
    }


    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
