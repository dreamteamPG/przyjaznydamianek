package com.przyjaznydamianek.sqlCreate;

/**
 * Created by Chris on 10/21/2014.
 */
public final class AK_PL extends BaseSQL {

    public static final String TABLE_NAME="AK_PL";
    public static final String ID = "ID";
    public static final String ID_PL = "ID_PL";
    public static final String ID_AK = "ID_AK";
    public static final String POZ_AK = "POZ_AK";
    public static final String TYP_AK = "TYP_AK";
    public static final String STATUS_AK = "STATUS_AK";


    public AK_PL(){
        super();
    }

    public static final String[] ALL_COLUMNS = {ID,ID_PL,ID_AK,POZ_AK,TYP_AK, STATUS_AK};

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String createSql() {

        return createTable(TABLE_NAME)
                .addField(ID, TEXT_FIELD, PRIMARY_KEY)
                .addField(ID_PL, TEXT_FIELD, REFERENCES(Plan.TABLE_NAME, Plan.ID))
                .addField(ID_AK, TEXT_FIELD, REFERENCES(Aktywnosc.TABLE_NAME, Aktywnosc.ID))
                .addField(POZ_AK, INTEGER_FIELD, NOT_NULL)
                .addField(TYP_AK, TEXT_FIELD)
                .addField(STATUS_AK, TEXT_FIELD)
                .finishCreateSql();
    }



}
