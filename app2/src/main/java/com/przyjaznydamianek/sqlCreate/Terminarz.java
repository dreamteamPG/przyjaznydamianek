package com.przyjaznydamianek.sqlCreate;

/**
 * Created by Chris on 10/21/2014.
 */
public final class Terminarz extends BaseSQL{

    public static final String TABLE_NAME="TERMINARZ";
    public static final String ID="ID";
    public static final String ID_PL="ID_PL";
    public static final String ID_USR="ID_USR";
    public static final String DATE="DATA";

    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID,TEXT_FIELD,PRIMARY_KEY)
                .addField(ID_PL,TEXT_FIELD,REFERENCES(Plan.TABLE_NAME,Plan.ID))
                .addField(ID_USR, TEXT_FIELD, REFERENCES(Uzytkownik.TABLE_NAME, Uzytkownik.ID))
                .addField(DATE, DATE_FIELD, NOT_NULL).finishCreateSql();
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
