package com.przyjaznydamianek.sqlCreate;

/**
 * Created by chris on 30.12.14.
 */
public class ChoosenUser extends BaseSQL {

    public static final String TABLE_NAME="CHOOSEN_USER";
    public static final String ID_ROW = "ID_ROW";
    public static final String ID_USER = "ID_USER";


    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID_ROW,TEXT_FIELD,PRIMARY_KEY)
                .addField(ID_USER,TEXT_FIELD,REFERENCES(Uzytkownik.TABLE_NAME,Uzytkownik.ID))
                .finishCreateSql();
    }


}
