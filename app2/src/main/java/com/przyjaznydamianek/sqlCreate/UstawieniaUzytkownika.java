package com.przyjaznydamianek.sqlCreate;

/**
 * Created by chris on 28.12.14.
 */
public class UstawieniaUzytkownika extends BaseSQL{

    public static final String TABLE_NAME = "USTAWIENIA_UZYTKOWNIKA";
    public static final String ID = "ID";
    public static final String TYP_WIDOK_AKTYWNOSCI = "A_VIEW_TYPE";
    public static final String TYP_WIDOK_CZYNNOSCI = "C_VIEW_TYPE";
    public static final String TYP_WIDOK_PLAN = "P_VIEW_TYPE";
    public static final String TIMER_SOUND_PATH = "TIMER_SOUND_PATH";

    public static final String[] ALL_COLUMNS={ID,TYP_WIDOK_AKTYWNOSCI,TYP_WIDOK_CZYNNOSCI,TYP_WIDOK_PLAN, TIMER_SOUND_PATH};

    @Override
    public String createSql() {
        return createTable(TABLE_NAME)
                .addField(ID,TEXT_FIELD,PRIMARY_KEY)
                .addField(TYP_WIDOK_AKTYWNOSCI,TEXT_FIELD)
                .addField(TYP_WIDOK_CZYNNOSCI, TEXT_FIELD)
                .addField(TYP_WIDOK_PLAN, TEXT_FIELD)
                .addField(TIMER_SOUND_PATH,TEXT_FIELD)
                .finishCreateSql();
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

}