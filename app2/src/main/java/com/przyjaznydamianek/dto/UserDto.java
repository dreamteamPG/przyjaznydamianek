package com.przyjaznydamianek.dto;

import android.content.ContentValues;

import com.przyjaznydamianek.models.User;
import com.przyjaznydamianek.models.UserPreferences;
import com.przyjaznydamianek.sqlCreate.Uzytkownik;

/**
 * Created by Chris on 10/24/2014.
 */
public class UserDto extends BaseDto {

    private User user;
    private UserPreferences preferences;

    public UserDto(){super(new Uzytkownik());}

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getID() {
        return user.getId();
    }

    @Override
    public String getCOLUMN_ID() {
        return Uzytkownik.ID;
    }

    public void setColumnsToRead(String[] columnsToRead) {
        this.columnsToRead = columnsToRead;
    }

    public void setContentValues(ContentValues contentValues) {
        this.contentValues = contentValues;
    }

    @Override
    public String[] getColumnsToRead() {
        if(columnsToRead==null){
            return Uzytkownik.ALL_COLUMNS;
        }
        return columnsToRead;
    }

    /**
     * Domysle zwraca contentValues calego modelu
     * @return
     */
    public ContentValues getContentValues(){
        if(contentValues==null) {
            contentValues = new ContentValues();
            contentValues.put(Uzytkownik.ID, user.getId());
            contentValues.put(Uzytkownik.NAME, user.getName());
            contentValues.put(Uzytkownik.SURNAME, user.getSurname());
        }
        return contentValues;
    }




}
