package com.przyjaznydamianek.dto;

import android.content.ContentValues;

import com.przyjaznydamianek.models.Plan;

/**
 * Created by Chris on 10/24/2014.
 */
public class PlanDto extends BaseDto {

    public PlanDto(){
        super(new com.przyjaznydamianek.sqlCreate.Plan());
    }

    private Plan plan;

    public boolean createMode = false;

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    @Override
    public String getID() {
        return this.plan.getId()+"";
    }

    @Override
    public String getCOLUMN_ID() {
        return com.przyjaznydamianek.sqlCreate.Plan.ID;
    }

    //TODO
    @Override
    public String[] getColumnsToRead() {
        if(columnsToRead==null){
            return com.przyjaznydamianek.sqlCreate.Plan.PLAN_ALL_COLUMNS;
        }
        return columnsToRead;
    }

    //TODO
    @Override
    public ContentValues getContentValues() {
        if(contentValues==null){
            contentValues=new ContentValues();
            if(this.createMode == false)
                contentValues.put(com.przyjaznydamianek.sqlCreate.Plan.ID,this.plan.getId());
            contentValues.put(com.przyjaznydamianek.sqlCreate.Plan.NAZWA,this.plan.getTitle());
        }
        return contentValues;
    }

    @Override
    public void setColumnsToRead(String[] columns) {
        this.columnsToRead=columns;
    }

    @Override
    public void setContentValues(ContentValues values) {
        this.contentValues=values;
    }
}
