package com.przyjaznydamianek.dto;

import android.content.ContentValues;

import com.przyjaznydamianek.sqlCreate.Terminarz;

/**
 * Created by Chris on 10/24/2014.
 */
public class TerminarzDto extends BaseDto {

    public TerminarzDto(){

    }
    @Override
    public String getID() {
        return null;
    }

    @Override
    public String getCOLUMN_ID() {
        return Terminarz.ID;
    }

    //TODO
    @Override
    public String[] getColumnsToRead() {
        if(columnsToRead==null) return null;
        else return columnsToRead;
    }

    //TODO
    @Override
    public ContentValues getContentValues() {
        if(contentValues==null){
                return null;
        }else{
            return contentValues;
        }

    }

    @Override
    public void setColumnsToRead(String[] columns) {
        this.columnsToRead=columns;
    }

    @Override
    public void setContentValues(ContentValues values) {
        this.contentValues=values;
    }
}
