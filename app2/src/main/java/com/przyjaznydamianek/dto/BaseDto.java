package com.przyjaznydamianek.dto;

import android.content.ContentValues;

import com.przyjaznydamianek.sqlCreate.BaseSQL;

/**
 * Created by Chris on 10/24/2014.
 */
public abstract class BaseDto {


    protected BaseSQL table;
    protected String[] columnsToRead;
    protected ContentValues contentValues;
    public BaseDto(){}

    public BaseDto(BaseSQL baseSQL){

        table=baseSQL;
    }

    public abstract String getID();
    public abstract  String getCOLUMN_ID();


    public BaseSQL getTable() {
        return table;
    }

    public void setTable(BaseSQL table) {
        this.table = table;
    }

    public abstract String[] getColumnsToRead();
    public abstract ContentValues getContentValues();
    public abstract void setColumnsToRead(String[] columns);
    public abstract void setContentValues(ContentValues values);

    public  String getSelectionString(){
        return getCOLUMN_ID()+"='"+getID()+"'";
    };

    public  String[] getSelectionArgs(){
        return null;
    }

    public  String groupBy(){
        return null;
    }

    public String having(){
        return null;
    }

    public String orderBy(){
        return null;
    }
}
