package com.przyjaznydamianek.dto;

import android.content.ContentValues;

import com.przyjaznydamianek.models.Activity;
import com.przyjaznydamianek.sqlCreate.Aktywnosc;

/**
 * Created by Chris on 10/24/2014.
 */
public class ActivityDto extends BaseDto {

    private Activity activity;

    public boolean createMode = false;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public ActivityDto(){
        super(new Aktywnosc());
    }

    @Override
    public String getID() {
        return ""+activity.getId();
    }

    @Override
    public String getCOLUMN_ID() {
        return Aktywnosc.ID;
    }

    @Override
    public String[] getColumnsToRead() {
        if(columnsToRead==null) return Aktywnosc.COLUMN_CHILD_ACTIVITY_ALL_COLUMNS;
        else return columnsToRead;
    }

    /**
     * Domyslniee contentValues odwzorowuje wszystkie pola na bazie utworzonego modelu
     * @return
     */
    @Override
    public ContentValues getContentValues() {
        if(contentValues==null){

            contentValues=new ContentValues();
            if(this.createMode == false)
                contentValues.put(Aktywnosc.ID,this.getID());
            contentValues.put(Aktywnosc.COLUMN_AUDIO,activity.getAudioPath());
            contentValues.put(Aktywnosc.COLUMN_TITLE,activity.getTitle());
            contentValues.put(Aktywnosc.COLUMN_TIME,activity.getTime());
            contentValues.put(Aktywnosc.COLUMN_ACTIVITY_LAST_SLIDE_NUMBER,activity.getLastSlideNumber());
            contentValues.put(Aktywnosc.COLUMN_ICON,activity.getIconPath());
            contentValues.put(Aktywnosc.COLUMN_STATUS,activity.getStatus());
            contentValues.put(Aktywnosc.COLUMN_TYPE_FLAG,activity.getTypeFlag());
            contentValues.put(Aktywnosc.COLUMN_ACTIVITY_NUMBER,activity.getNumber());
            contentValues.put(Aktywnosc.COLUMN_DATE,activity.getDate());

            return contentValues;
        }
        return contentValues;
    }

    @Override
    public void setColumnsToRead(String[] columns) {
        this.columnsToRead=columns;
    }

    @Override
    public void setContentValues(ContentValues values) {
        this.contentValues=values;
    }
}
