package com.przyjaznydamianek.dto;

import android.content.ContentValues;

import com.przyjaznydamianek.sqlCreate.Czynnosc;
import com.przyjaznydamianek.models.Slide;

/**
 * Created by Chris on 10/24/2014.
 */
public class SlideDto extends  BaseDto {

    public Slide getSlide() {
        return slide;
    }

    public void setSlide(Slide slide) {
        this.slide = slide;
    }

    private Slide slide;

    public boolean createMode = false;

    public SlideDto(){
        super(new Czynnosc());
    }

    @Override
    public String getID() {
        return slide.getId();
    }

    @Override
    public String getCOLUMN_ID() {
        return Czynnosc.ID;
    }

    @Override
    public String[] getColumnsToRead() {
        if(columnsToRead==null){
            return Czynnosc.COLUMN_SLIDE_ALL_COLUMNS;
        }else{
            return columnsToRead;
        }
    }

    /**
     * Domysle contentValues zwraca odwzorowanie z modelu
     * @return
     */
    @Override
    public ContentValues getContentValues() {
        if(contentValues==null){
            contentValues = new ContentValues();
            if(createMode==false){
                contentValues.put(Czynnosc.ID,getID());
            }
            contentValues.put(Czynnosc.TEXT,slide.getText());
            contentValues.put(Czynnosc.AUDIO,slide.getAudioPath());
            contentValues.put(Czynnosc.IMAGE,slide.getImagePath());
            contentValues.put(Czynnosc.STATUS,slide.getStatus());
            contentValues.put(Czynnosc.TIMER,slide.getTime());
            return contentValues;
        }
        return contentValues;
    }

    @Override
    public void setColumnsToRead(String[] columns) {
        this.columnsToRead=columns;
    }

    @Override
    public void setContentValues(ContentValues values) {
        this.contentValues=values;
    }
}
