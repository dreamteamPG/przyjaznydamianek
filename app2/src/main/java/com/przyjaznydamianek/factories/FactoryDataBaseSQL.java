package com.przyjaznydamianek.factories;

import android.database.sqlite.SQLiteDatabase;

import com.przyjaznydamianek.sqlCreate.AK_PL;
import com.przyjaznydamianek.sqlCreate.Aktywnosc;
import com.przyjaznydamianek.sqlCreate.CZ_AK;
import com.przyjaznydamianek.sqlCreate.ChoosenUser;
import com.przyjaznydamianek.sqlCreate.Czynnosc;
import com.przyjaznydamianek.sqlCreate.Plan;
import com.przyjaznydamianek.sqlCreate.Terminarz;
import com.przyjaznydamianek.sqlCreate.UST_USER;
import com.przyjaznydamianek.sqlCreate.UstawieniaUzytkownika;
import com.przyjaznydamianek.sqlCreate.Uzytkownik;

/**
 * Created by Chris on 10/24/2014.
 */
public class FactoryDataBaseSQL {
    public FactoryDataBaseSQL(){

    }

    public void CreateDatabaseSQL(SQLiteDatabase db){

try {
    db.execSQL(new Czynnosc().createSql());
    db.execSQL(new Aktywnosc().createSql());
    db.execSQL(new CZ_AK().createSql());
    db.execSQL(new Uzytkownik().createSql());
    db.execSQL(new Plan().createSql());
    db.execSQL(new Terminarz().createSql());
    db.execSQL(new AK_PL().createSql());
    db.execSQL(new UstawieniaUzytkownika().createSql());
    db.execSQL(new UST_USER().createSql());
    db.execSQL(new ChoosenUser().createSql());
}catch (Exception e){
    System.out.println(e.getMessage());
}

    }

    public void DropDatabaseSql(SQLiteDatabase db){

        try {
            db.execSQL(new Czynnosc().dropSql());
            db.execSQL(new Aktywnosc().dropSql());
            db.execSQL(new CZ_AK().dropSql());
            db.execSQL(new Uzytkownik().dropSql());
            db.execSQL(new Plan().dropSql());
            db.execSQL(new Terminarz().dropSql());
            db.execSQL(new AK_PL().dropSql());
            db.execSQL(new UstawieniaUzytkownika().dropSql());
            db.execSQL(new UST_USER().dropSql());
            db.execSQL(new ChoosenUser().dropSql());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }


}
