package com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils;

/**
 * Created by Michal on 2014-12-26.
 */
public class RequestCodes {
    public static final int ACTIVITY_ADDED = 1;
    public static final int ACTIVITY_EDITED = 2;
    public static final int ZARZADZAJ_CZYNNOSCIAMI = 3;
    public static final int DODAJ_NOWA_AKTYWNOSC = 4;
    public static final int EDYTUJ_ISTNIEJACA_AKTYWNOSC = 5;
    public static final int USUN_AKTYWNOSC = 6;
    public static final int SLIDE_ADDED = 7;
    public static final int SLIDE_EDITED = 8;
    public static final int DODAJ_NOWA_CZYNNOSC = 9;
    public static final int EDYTUJ_CZYNNOSC = 10;
    public static final int PLAN_ADDED = 11;
    public static final int PLAN_EDITED = 12;
    public static final int PLAN_DODAJ_AKTYWNOSC = 13;
    public static final int PLAN_AKTYWNOSC = 14;
    public static final int PLAN_PRZERWA = 15;
    public static final int PLAN_GALERIA = 16;
    public static final int PLAN_NOWA_AKTYWNOSC = 17;
    public static final int ZARZADZANIE_AKTYWNOSCIAMI = 18;
    public static final int SZABLONY_PLANOW = 19;
    public static final int PRZEGLADANIE_PRZERWY = 20;
    public static final int PRZEGLADANIE_GA = 21;
    public static final int USUN_PLAN = 22;
    public static final int EDYTUJ_PLAN = 23;
    public static final int AKTUALNY_PLAN_EDYCJA = 24;
    public static final int AKTUALNY_PLAN_PRZEGLADANIE= 25;
    public static final int DODAJ_AKTYWNOSCI_Z_SZABLONU=26;
    public static final int ZARZADZAJ_PLANEM_UZYTKOWNIKA = 27;
    public static final int ZARZADZANIE_UZYTKOWNIKAMI = 28;
    public static final int EDYCJA_USERA = 29;
    public static final int TWORZENIE_USERA=30;
    public static final int FILE_CHOOSER = 31;
    public static final int FILE_CHOOSER_OBRAZKI = 32;
    public static final int FILE_CHOOSER_DZWIEKI = 33;
    public static final int DODAJ_WYBRANE_Z_PLANU_SZUKANIE = 34;
    public static final int DODAJ_WYBRANE_Z_PLANU = 35;
    public static final int DODAJ_WYBRANE_Z_PLANU_GA = 36;
    public static final int DODAJ_WYBRANE_Z_PLANU_PRZERWY = 37;
}
