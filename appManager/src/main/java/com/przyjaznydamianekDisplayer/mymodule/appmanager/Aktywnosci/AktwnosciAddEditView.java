package com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.dao.ActivityDao;
import com.przyjaznydamianek.dto.ActivityDto;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Czynnosci.ZarzadzanieCzynnosciamiView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.FileDialog;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

import java.io.File;
import java.util.ArrayList;

import br.com.thinkti.android.filechooser.FileChooser;

/**
 * Created by Michal on 2014-11-16.
 */
public class AktwnosciAddEditView extends Activity {
    private String pathToPicture;
    private com.przyjaznydamianek.models.Activity aktywnosc;
    private MediaPlayer mp;
    private ActivityDao activityDao;
    private int mode = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aktywnosciaddeditview);
        if(getIntent().getExtras()!=null && getIntent().getExtras().get("ACTIVITY")!=null){
            mode = RequestCodes.ACTIVITY_EDITED;
            this.aktywnosc = (com.przyjaznydamianek.models.Activity)getIntent().getExtras().get("ACTIVITY");
            if(this.aktywnosc.getIconPath()!=null && !this.aktywnosc.getIconPath().equals("")){
                setBMP(this.aktywnosc.getIconPath());
            }
            if(aktywnosc.getAudioPath()!=null && !aktywnosc.getAudioPath().equals("")){
                ImageView usunDzwiekIcon = (ImageView) (findViewById(R.id.imageView3));
                usunDzwiekIcon.setVisibility(View.VISIBLE);
            }
            EditText etName = (EditText) findViewById(R.id.editText);
            etName.setText(this.aktywnosc.getTitle());
            EditText etTime = (EditText) findViewById(R.id.editText2);
            etTime.setText(String.valueOf(this.aktywnosc.getTime()));
            TextView tv = (TextView) findViewById(R.id.textView5);
            if(this.aktywnosc.getSlides()==null){
                tv.setText("0");
            } else {
                tv.setText(this.aktywnosc.getSlides().size() + "");
            }
        }
        else {
            mode = RequestCodes.ACTIVITY_ADDED;
            this.aktywnosc = new com.przyjaznydamianek.models.Activity();
        }
        try {
            activityDao = new ActivityDao(MySQLiteHelper.getDb());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setBMP(String pathToPicture){
        try {
            Bitmap bmp = BitmapFactory.decodeFile(pathToPicture);
            Bitmap scaledBmp = Bitmap.createScaledBitmap(bmp, 100, 100, false);
            ImageView activityImage = (ImageView) (findViewById(R.id.imageView));
            activityImage.setImageBitmap(scaledBmp);
            aktywnosc.setIconPath(pathToPicture);
            ImageView usunObrazIcon = (ImageView) (findViewById(R.id.imageView4));
            usunObrazIcon.setVisibility(View.VISIBLE);
        }catch (Exception e){
            pathToPicture = "";
            aktywnosc.setIconPath(pathToPicture);
        }
    }

    public void ustawObraz(View v){
        //Intent intent = new Intent(this, FileDialogView.class);
        //startActivityForResult(intent,DODAJ_OBRAZ);
       /* File mPath = new File(Environment.getExternalStorageDirectory()+"");
        FileDialog fileDialog = new FileDialog(this, mPath);
        //fileDialog.setFileEndsWith(".txt");
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            public void fileSelected(File file) {
                Log.d(getClass().getName(), "selected file " + file.toString());
                pathToPicture=file.toString();
                try {
                    setBMP(pathToPicture);
                }catch (Exception e){

                }
            }
        });
        fileDialog.showDialog();*/
        Intent intent = new Intent(this, FileChooser.class);
        ArrayList<String> extensions = new ArrayList<String>();
        extensions.add(".jpg");
        extensions.add(".jpeg");
        extensions.add(".png");
        intent.putStringArrayListExtra("filterFileExtension", extensions);
        intent.putExtra("fileStartPath", Environment.getExternalStorageDirectory());
        startActivityForResult(intent, RequestCodes.FILE_CHOOSER_OBRAZKI);
    }

    public void ustawDzwiek(View v){
        //Intent intent = new Intent(this, FileDialogView.class);
        //startActivityForResult(intent,DODAJ_OBRAZ);
       /* File mPath = new File(Environment.getExternalStorageDirectory()+"");
        FileDialog fileDialog = new FileDialog(this, mPath);
        fileDialog.setFileEndsWith(".mp3");
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            public void fileSelected(File file) {
                Log.d(getClass().getName(), "selected file " + file.toString());
                try {
                    aktywnosc.setAudioPath(file.toString());
                    ImageView usunDzwiekIcon = (ImageView) (findViewById(R.id.imageView3));
                    usunDzwiekIcon.setVisibility(View.VISIBLE);
                }catch (Exception e){

                }
            }
        });
        fileDialog.showDialog();*/
        Intent intent = new Intent(this, FileChooser.class);
        ArrayList<String> extensions = new ArrayList<String>();
        extensions.add(".mp3");
        intent.putStringArrayListExtra("filterFileExtension", extensions);
        intent.putExtra("fileStartPath", Environment.getExternalStorageDirectory());
        startActivityForResult(intent, RequestCodes.FILE_CHOOSER_DZWIEKI);
    }
    public void usunObraz(View v) {
        aktywnosc.setIconPath(null);
        ImageView activityImage = (ImageView) (findViewById(R.id.imageView));
        activityImage.setImageResource(R.drawable.t1);
        ImageView usunObrazIcon = (ImageView) (findViewById(R.id.imageView4));
        usunObrazIcon.setVisibility(View.INVISIBLE);
    }

    public void usunDzwiek(View v){
        aktywnosc.setAudioPath(null);
        ImageView usunDzwiekIcon = (ImageView) (findViewById(R.id.imageView3));
        usunDzwiekIcon.setVisibility(View.INVISIBLE);
    }

    public void playSound(View v){
        if(aktywnosc.getAudioPath()!=null && !aktywnosc.getAudioPath().equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(aktywnosc.getAudioPath()));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }

    public void zarzadzanieAktywnosciamiClick(View v){
        Intent intent = new Intent(this, ZarzadzanieCzynnosciamiView.class);
        intent.putExtra("ACTIVITY",this.aktywnosc);
        startActivityForResult(intent, RequestCodes.ZARZADZAJ_CZYNNOSCIAMI);
    }

    public void zapisz(View v){
        String name="";
        int time=0;
        EditText etName = (EditText) findViewById(R.id.editText);
        try {
            name = etName.getText().toString();
        } catch (Exception e){

        }
        EditText etTime = (EditText) findViewById(R.id.editText2);
        try {
            time = Integer.parseInt(etTime.getText().toString());
        } catch (Exception e){

        }
        try{
            if(name.length()!=0){
                aktywnosc.setTitle(name);
            } else
            {
                return;
            }
            aktywnosc.setTime(time);
            ActivityDto adto = new ActivityDto();
            aktywnosc.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.ACTIVITY+"");
            aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW+"");
            adto.setActivity(aktywnosc);
            if(mode == RequestCodes.ACTIVITY_ADDED){
                activityDao.create(adto);
            }
            if(mode == RequestCodes.ACTIVITY_EDITED){
                activityDao.update(adto);
            }
            Intent intent = new Intent();
            intent.putExtra("ACTIVITY", this.aktywnosc);
            setResult(mode , intent);
            super.finish();
            super.finish();
        } catch(Exception e){

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RequestCodes.ZARZADZAJ_CZYNNOSCIAMI && resultCode == RequestCodes.ZARZADZAJ_CZYNNOSCIAMI) {
            com.przyjaznydamianek.models.Activity result = (com.przyjaznydamianek.models.Activity) data.getExtras().get("ACTIVITY");
            this.aktywnosc.setSlides(result.getSlides());
            TextView tv = (TextView) findViewById(R.id.textView5);
            tv.setText(this.aktywnosc.getSlides().size() + "");
        }
        if(requestCode == RequestCodes.FILE_CHOOSER_OBRAZKI && resultCode == -1){
            String fileSelected = data.getStringExtra("fileSelected");
            if(!("".equals(fileSelected))){
                try {
                    pathToPicture=fileSelected;
                    setBMP(pathToPicture);
                }catch (Exception e){

                }
            }
            Toast.makeText(this, fileSelected, Toast.LENGTH_SHORT).show();
        }
        if(requestCode == RequestCodes.FILE_CHOOSER_DZWIEKI && resultCode == -1){
            String fileSelected = data.getStringExtra("fileSelected");
            if(!("".equals(fileSelected))){
                try {
                    aktywnosc.setAudioPath(fileSelected.toString());
                    ImageView usunDzwiekIcon = (ImageView) (findViewById(R.id.imageView3));
                    usunDzwiekIcon.setVisibility(View.VISIBLE);
                }catch (Exception e){

                }
            }
            Toast.makeText(this, fileSelected, Toast.LENGTH_SHORT).show();
        }
    }
}
