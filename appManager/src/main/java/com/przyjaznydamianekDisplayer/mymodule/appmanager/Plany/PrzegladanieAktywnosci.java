package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianek.utils.BusinessLogic;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci.AktwnosciAddEditView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivityAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

import java.util.List;

/**
 * Created by Michal on 2014-12-26.
 */
public class PrzegladanieAktywnosci extends Activity implements AdapterView.OnItemClickListener {

    private ActivityAdapter listAdapter;
    private ListView mainListView;
    private MediaPlayer mp;
    private List<com.przyjaznydamianek.models.Activity> aList;
    private Integer mode;
    private Boolean changeStatusEnabled = false;
    private Plan plan;
    private int editedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planprzegladanieaktywnoosci);

        mode = (Integer) getIntent().getExtras().get("REQUESTCODE");
        plan = (Plan) getIntent().getExtras().get("PLAN");
        if(getIntent().getExtras().get("EDYCJAWYKONANIA")!=null){
            changeStatusEnabled = true;
        }
        TextView text = (TextView) (findViewById(R.id.textView));
        if(mode == RequestCodes.PRZEGLADANIE_PRZERWY){
            text.setText("Aktywności przerwy:");
            this.aList = plan.getAktywnosciPrzerwy();
        } else {
            text.setText("Aktywności galerii aktywności:");
            this.aList = plan.getAktywnosciGaleriiAktywnosci();
        }
        mainListView = (ListView) findViewById(R.id.listView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initList();
    }

    public void initList(){
        listAdapter = new ActivityAdapter(this, R.layout.rowlistlayout,R.id.label, aList);
        mainListView.setAdapter(listAdapter);
        mainListView.setOnItemClickListener(this);
    }

    public void edytujSlajd(View v){
        Intent intent = new Intent(this, AktwnosciAddEditView.class);
        int position = Integer.parseInt(v.getTag().toString());
        editedPosition = position;
        com.przyjaznydamianek.models.Activity aktywnosc = aList.get(position);
        intent.putExtra("ACTIVITY", aktywnosc);
        startActivityForResult(intent,RequestCodes.ACTIVITY_EDITED);
    }

    public void usunSlajd(View v){
        int position = Integer.parseInt(v.getTag().toString());
        com.przyjaznydamianek.models.Activity aktywnosc = aList.get(position);
        boolean option = aktywnosc.getStatus()!=null && aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString());
        if(mode == RequestCodes.PRZEGLADANIE_GA){
            for(com.przyjaznydamianek.models.Activity act:plan.getActivities()){
                if(option){
                    if(act.getId().equals(aktywnosc.getId())&& act.getTypeFlag()!=null && act.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString())){
                        plan.getActivities().remove(act);
                        break;
                    }
                } else {
                    if(act.getId().equals( BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID)){
                        plan.getActivities().remove(act);
                        break;
                    }
                }
            }
        }
        listAdapter.remove(position);
    }

    public void playSound(View v){
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }
    @Override
    public void finish(){
        if(mode == RequestCodes.PRZEGLADANIE_PRZERWY){
           plan.setAktywnosciPrzerwy(this.aList);
        } else {
           plan.setAktywnosciGaleriiAktywnosci( this.aList);
        }
        Intent intent = new Intent();
        intent.putExtra("PLAN", this.plan);
        setResult(mode , intent);
        super.finish();
    }


    private void refreshAllTheSameActivities(com.przyjaznydamianek.models.Activity editedActivity){
        for(int i = 0;i<this.plan.getActivities().size();i++){
            if(this.plan.getActivities().get(i).getId().equals(editedActivity.getId())){
                this.plan.getActivities().set(i,editedActivity);
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciGaleriiAktywnosci().size();i++){
            if(this.plan.getAktywnosciGaleriiAktywnosci().get(i).getId().equals(editedActivity.getId())){
                this.plan.getAktywnosciGaleriiAktywnosci().set(i,editedActivity);
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciPrzerwy().size();i++){
            if(this.plan.getAktywnosciPrzerwy().get(i).getId().equals(editedActivity.getId())){
                this.plan.getAktywnosciPrzerwy().set(i,editedActivity);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data!=null && data.getExtras()!=null && data.getExtras().get("ACTIVITY")!=null) {
            if(requestCode == RequestCodes.ACTIVITY_EDITED && resultCode ==  RequestCodes.ACTIVITY_EDITED){
                com.przyjaznydamianek.models.Activity editedActivity = (com.przyjaznydamianek.models.Activity)data.getExtras().get("ACTIVITY");
                refreshAllTheSameActivities(editedActivity);
                Toast.makeText(this, "Edytowano aktywnosc", Toast.LENGTH_LONG).show();
            }
        }

        editedPosition = 0;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(changeStatusEnabled ==null || changeStatusEnabled == false){
            return;
        }
        com.przyjaznydamianek.models.Activity aktywnosc = (com.przyjaznydamianek.models.Activity) mainListView.getAdapter().getItem(i);
        if(aktywnosc.getStatus()!=null && aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())) {
            aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
            if(mode == RequestCodes.PRZEGLADANIE_GA){
                for(int j = 0;i<this.plan.getActivities().size();j++){
                    if(this.plan.getActivities().get(j).getId().equals(aktywnosc.getId()) && this.plan.getActivities().get(j).getTypeFlag()!= null && this.plan.getActivities().get(j).getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString())){
                        aktywnosc = new com.przyjaznydamianek.models.Activity();
                        aktywnosc.setId(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID);
                        aktywnosc.setTitle("Galeria aktywności");
                        aktywnosc.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString());
                        this.plan.getActivities().set(j,aktywnosc);
                        break;
                    }
                }
            }
        } else {
            if(aktywnosc.getStatus()== null || !aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString());
                if(mode == RequestCodes.PRZEGLADANIE_GA){
                    for(int j = 0;i<this.plan.getActivities().size();j++){
                        if(this.plan.getActivities().get(j).getTypeFlag()!= null && this.plan.getActivities().get(j).getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString())){
                            this.plan.getActivities().set(j, aktywnosc);
                            this.plan.getActivities().get(j).setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString());
                            break;
                        }
                    }
                }
            }
        }
        listAdapter.notifyDataSetChanged();

    }
}
