package com.przyjaznydamianekDisplayer.mymodule.appmanager.PlanyZarzadzanie;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany.PlanyAddEditView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany.ZnajdzPlanView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

/**
 * Created by Michal on 2014-12-27.
 */
public class PlanyZarzadzanieMainView extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planyzarzadzaniemainview);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void przydzielEdytujAktualnyPlanClick(View v) {
        Intent intent = new Intent(this, PrzydzielEdytujAktualnyPlan.class);
        startActivityForResult(intent, RequestCodes.AKTUALNY_PLAN_EDYCJA);
    }

    public void przeglądajAktualnyPlanClick(View v) {
        Intent intent = new Intent(this, PrzegladajAktualnyPlan.class);
        startActivityForResult(intent, RequestCodes.AKTUALNY_PLAN_PRZEGLADANIE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO
    }
}
