package com.przyjaznydamianekDisplayer.mymodule.appmanager.Informacje;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.przyjaznydamianek.models.Activity;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;

/**
 * Created by Michal on 2015-04-27.
 */
public class OProgramieView extends android.app.Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.oprogramieview);
    }
}
