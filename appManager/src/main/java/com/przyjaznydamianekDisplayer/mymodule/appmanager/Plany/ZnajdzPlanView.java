package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.dao.ActivityDao;
import com.przyjaznydamianek.dao.PlanDao;
import com.przyjaznydamianek.dto.ActivityDto;
import com.przyjaznydamianek.dto.PlanDto;
import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivitySimpleAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2014-12-27.
 */
public class ZnajdzPlanView extends Activity implements AdapterView.OnItemClickListener {

    private ListView mainListView ;
    private PlanDao planDao;
    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.znajdzplanview);
        try {
            mainListView = (ListView) findViewById(R.id.listView);
            planDao = new PlanDao(MySQLiteHelper.getDb());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        mode = (Integer)getIntent().getExtras().get("REQUESTCODE");
    }

    public void znajdzClick(View v){
        String name="";
        EditText etName = (EditText) findViewById(R.id.editText);
        try {
            name = etName.getText().toString();
        } catch (Exception e){
        }

        Toast.makeText(v.getContext(), "Searching for: " + name, Toast.LENGTH_LONG).show();

        List<Plan> list = planDao.getPlanByTitle(name);

        ArrayAdapter<Plan> adapter = new ArrayAdapter<Plan>(this,
                R.layout.simplerowlistview, R.id.label, list);

        mainListView.setAdapter(adapter);

        mainListView.setOnItemClickListener((AdapterView.OnItemClickListener) this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class DialogInterfaceOnClickHelper implements DialogInterface.OnClickListener{
        private Plan planToRemove;
        private Activity actvity;

        public void setActivityToRemoveId(Plan pl, Activity actvity){
            this.planToRemove = pl;
            this.actvity = actvity;
        }
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    PlanDto pdto = new PlanDto();
                    pdto.setPlan(planToRemove);
                    planDao.delete(pdto);
                    this.actvity.finish();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Plan item = (Plan) mainListView.getAdapter().getItem(i);
        if(mode== RequestCodes.USUN_PLAN){
            DialogInterfaceOnClickHelper dialogClickListener = new DialogInterfaceOnClickHelper();
            dialogClickListener.setActivityToRemoveId(item, this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Czy na pewno skasować plan \""+ item.getTitle() +"\" ?\n\n\n")
                    .setPositiveButton("Tak", dialogClickListener)
                    .setNegativeButton("Nie", dialogClickListener)
                    .show();
        } else{
            if( mode == RequestCodes.DODAJ_WYBRANE_Z_PLANU_SZUKANIE) {
                Intent intent = new Intent(this, PlanyDodajWybraneView.class);
                intent.putExtra("PLAN",item);
                startActivityForResult(intent, RequestCodes.DODAJ_WYBRANE_Z_PLANU);
            } else {
                Intent intent = new Intent(this, PlanyAddEditView.class);
                intent.putExtra("PLAN",item);
                if(mode==RequestCodes.EDYTUJ_PLAN){
                    startActivityForResult(intent, RequestCodes.EDYTUJ_PLAN);
                }
                if(mode==RequestCodes.DODAJ_AKTYWNOSCI_Z_SZABLONU){
                    setResult(mode, intent);
                    super.finish();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RequestCodes.DODAJ_WYBRANE_Z_PLANU && resultCode== 1){
            if(data!= null && data.getExtras()!=null && data.getExtras().get("ACTIVITIES")!=null){
                Intent intent = new Intent();
                intent.putExtra("ACTIVITIES", (ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("ACTIVITIES"));
                intent.putExtra("BREAKACTIVITIES", (ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("BREAKACTIVITIES"));
                intent.putExtra("GAACTIVITIES", (ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("GAACTIVITIES"));
                setResult(requestCode, intent);
            }
        }
        super.finish();
    }
}
