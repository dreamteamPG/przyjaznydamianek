package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci.AktwnosciAddEditView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci.ZnajdzAktywnosciView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

/**
 * Created by Michal on 2014-12-26.
 */
public class PlanyDodajAktywnoscView extends Activity {
    private Integer mode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planydodajaktywnoscview);
        mode = (Integer)getIntent().getExtras().get("REQUESTCODE");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void nowaAktywnoscClick(View v) {
        Intent intent = new Intent(this, AktwnosciAddEditView.class);
        intent.putExtra("REQUESTCODE", RequestCodes.PLAN_NOWA_AKTYWNOSC);
        startActivityForResult(intent,RequestCodes.PLAN_NOWA_AKTYWNOSC);
    }

    public void istniejacaAktywnoscPlanuClick(View v) {
        Intent intent = new Intent(this, ZnajdzAktywnosciView.class);
        intent.putExtra("REQUESTCODE", RequestCodes.PLAN_AKTYWNOSC);
        startActivityForResult(intent,RequestCodes.PLAN_AKTYWNOSC);
    }

    public void aktywnoscPrzerwyClick(View v) {
        Intent intent = new Intent(this, ZnajdzAktywnosciView.class);
        intent.putExtra("REQUESTCODE", RequestCodes.PLAN_PRZERWA);
        startActivityForResult(intent,RequestCodes.PLAN_PRZERWA);
    }

    public void aktywnoscGaleriiClick(View v) {
        Intent intent = new Intent(this, ZnajdzAktywnosciView.class);
        intent.putExtra("REQUESTCODE", RequestCodes.PLAN_GALERIA);
        startActivityForResult(intent,RequestCodes.PLAN_GALERIA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent();
        if(data!= null && data.getExtras()!=null && data.getExtras().get("ACTIVITY")!=null){
            intent.putExtra("ACTIVITY", (com.przyjaznydamianek.models.Activity)data.getExtras().get("ACTIVITY"));
        }
        setResult(requestCode, intent);
        super.finish();
    }
}
