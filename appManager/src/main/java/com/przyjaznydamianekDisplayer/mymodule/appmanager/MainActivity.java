package com.przyjaznydamianekDisplayer.mymodule.appmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.dao.ActivityDao;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci.AktywnosciMainView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Informacje.OProgramieView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany.PlanyMainView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.PlanyZarzadzanie.PlanyZarzadzanieMainView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Users.UserListView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void zarzadzanieAktywnosciamiClick(View v) {
        Intent intent = new Intent(this, AktywnosciMainView.class);
        startActivityForResult(intent, RequestCodes.ZARZADZANIE_AKTYWNOSCIAMI);
    }

    public void oProgramieClick(View v) {
        Intent intent = new Intent(this, OProgramieView.class);
        startActivityForResult(intent, 8642);
    }

    public void szablonyPlanowClick(View v){
        Intent intent = new Intent(this, PlanyMainView.class);
        startActivityForResult(intent, RequestCodes.SZABLONY_PLANOW);
    }

    public void zarzadzaniePlanemUzytkownikaClick(View v){
        Intent intent = new Intent(this, PlanyZarzadzanieMainView.class);
        startActivityForResult(intent, RequestCodes.ZARZADZAJ_PLANEM_UZYTKOWNIKA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO
    }

    public void zarzadzanieUzytkownikamiClick(View view) {

        Intent intent = new Intent(this,UserListView.class);
        startActivity(intent);
    }
}
