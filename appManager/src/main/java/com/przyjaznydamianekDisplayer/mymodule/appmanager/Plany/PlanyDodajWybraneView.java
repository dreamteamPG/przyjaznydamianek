package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivityAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivitySimpleAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

import java.util.ArrayList;

/**
 * Created by Michal on 2015-04-23.
 */
public class PlanyDodajWybraneView extends Activity implements AdapterView.OnItemClickListener {

    private Plan plan;
    private ArrayList<com.przyjaznydamianek.models.Activity> wybrane;
    private ArrayList<com.przyjaznydamianek.models.Activity> wybranePrzerwy;
    private ArrayList<com.przyjaznydamianek.models.Activity> wybraneGA;
    private ActivitySimpleAdapter listAdapter;
    private ListView mainListView;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plandodajwybraneview);
        try {
            this.plan = (Plan)getIntent().getExtras().get("PLAN");
            this.wybrane = new ArrayList<com.przyjaznydamianek.models.Activity>();
            this.wybranePrzerwy = new ArrayList<com.przyjaznydamianek.models.Activity>();
            this.wybraneGA = new ArrayList<com.przyjaznydamianek.models.Activity>();
            if(this.plan.getActivities()==null){
                this.plan.setActivities(new ArrayList<com.przyjaznydamianek.models.Activity>());
            }
            mainListView = (ListView) findViewById(R.id.listView);
            TextView tv =(TextView) findViewById(R.id.nazwaPlanu);
            if( tv!= null){
                tv.setText(this.plan.getTitle());
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            super.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initList();
    }

    public void initList(){
        listAdapter = new ActivitySimpleAdapter(this, R.layout.rowlistlayoutactivitysimple,R.id.label, this.plan.getActivities());
        mainListView.setAdapter(listAdapter);
        mainListView.setOnItemClickListener(this);
    }

    public void playSound(View v){
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        com.przyjaznydamianek.models.Activity aktywnosc = (com.przyjaznydamianek.models.Activity) mainListView.getAdapter().getItem(i);
        if(aktywnosc.getStatus()==null || !aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())) {
            if (aktywnosc.getTypeFlag() != null && aktywnosc.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString())) {
            } else {
                aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString());
                wybrane.add(aktywnosc);
                listAdapter.notifyDataSetChanged();
            }
        } else if (aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
            wybrane.remove(aktywnosc);
            aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
            listAdapter.notifyDataSetChanged();
        }
    }

    public void wybierzGA (View v){
        Intent intent = new Intent(this, PlanyDodawanieGAPrzerwy.class);
        intent.putExtra("ACTIVITIES", (ArrayList)this.plan.getAktywnosciGaleriiAktywnosci());
        startActivityForResult(intent,RequestCodes.DODAJ_WYBRANE_Z_PLANU_GA);
    }

    public void wybierzPrzerwy (View v){
        Intent intent = new Intent(this, PlanyDodawanieGAPrzerwy.class);
        intent.putExtra("ACTIVITIES", (ArrayList)this.plan.getAktywnosciPrzerwy());
        startActivityForResult(intent,RequestCodes.DODAJ_WYBRANE_Z_PLANU_PRZERWY);
    }

    public void zapisz (View v){
        Intent intent = new Intent();
        for(com.przyjaznydamianek.models.Activity ac : wybrane){
            ac.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
        }
        intent.putExtra("ACTIVITIES", wybrane);
        for(com.przyjaznydamianek.models.Activity ac : this.plan.getAktywnosciGaleriiAktywnosci()){
            if(ac.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                ac.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
                wybraneGA.add(ac);
            }
        }
        for(com.przyjaznydamianek.models.Activity ac : this.plan.getAktywnosciPrzerwy()){
            if(ac.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                ac.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
                wybranePrzerwy.add(ac);
            }
        }
        intent.putExtra("BREAKACTIVITIES", wybranePrzerwy);
        intent.putExtra("GAACTIVITIES", wybraneGA);
        setResult(1, intent);
        super.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null && data.getExtras().get("ACTIVITIES") != null){
            if(requestCode == RequestCodes.DODAJ_WYBRANE_Z_PLANU_GA) {
                this.plan.getAktywnosciGaleriiAktywnosci().clear();
                this.plan.getAktywnosciGaleriiAktywnosci().addAll((ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("ACTIVITIES"));
            } else if (requestCode == RequestCodes.DODAJ_WYBRANE_Z_PLANU_PRZERWY){
                this.plan.getAktywnosciPrzerwy().clear();
                this.plan.getAktywnosciPrzerwy().addAll((ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("ACTIVITIES"));
            }
        }
    }
}
