package com.przyjaznydamianekDisplayer.mymodule.appmanager.Czynnosci;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.przyjaznydamianek.dto.SlideDto;
import com.przyjaznydamianek.models.Slide;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.SlidesAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2014-11-17.
 */
public class ZarzadzanieCzynnosciamiView extends Activity {
    private SlidesAdapter listAdapter ;
    private com.przyjaznydamianek.models.Activity activity;
    private int activityMode;
    private ListView mainListView;
    private MediaPlayer mp;

    @Override
    protected void onResume(){
        super.onResume();
        initList();
    }

    public void initList(){
        listAdapter = new SlidesAdapter(this, R.layout.rowlistlayout,R.id.label, activity.getSlides());
        mainListView.setAdapter(listAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zarzadzanieczynnosciamiview);
        activity = (com.przyjaznydamianek.models.Activity)getIntent().getExtras().get("ACTIVITY");
        mainListView = (ListView) findViewById(R.id.listView);
        if(this.activity.getSlides()==null){
            List<Slide> ls = new ArrayList<Slide>();
            this.activity.setSlides(ls);
        }
    }

    public void dodajNowaCzynnoscClick(View v) {
        Intent intent = new Intent(this, CzynnosciAddEditView.class);
        startActivityForResult(intent, RequestCodes.DODAJ_NOWA_CZYNNOSC);
    }

    public void zapisz(View v) {
        Intent intent = new Intent();
        intent.putExtra("ACTIVITY", this.activity);
        setResult(RequestCodes.ZARZADZAJ_CZYNNOSCIAMI , intent);
        super.finish();
    }

    public void edytujSlajd(View v){
        Intent intent = new Intent(this, CzynnosciAddEditView.class);
        int position = Integer.parseInt(v.getTag().toString());
        intent.putExtra("SLIDE",activity.getSlides().get(position));
        intent.putExtra("POSITION",position);
        startActivityForResult(intent,RequestCodes.EDYTUJ_CZYNNOSC);
    }

    public void usunSlajd(View v){
        int position = Integer.parseInt(v.getTag().toString());
        listAdapter.remove(position);
    }

    public void playSound(View v){
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==RequestCodes.EDYTUJ_CZYNNOSC && resultCode == RequestCodes.SLIDE_EDITED){
            activity.getSlides().set(Integer.parseInt(data.getExtras().get("POSITION").toString()),(Slide)data.getExtras().get("SLIDE"));
        }
        if(requestCode==RequestCodes.DODAJ_NOWA_CZYNNOSC && resultCode == RequestCodes.SLIDE_ADDED){
            activity.getSlides().add((Slide)data.getExtras().get("SLIDE"));
        }
        initList();
    }
}
