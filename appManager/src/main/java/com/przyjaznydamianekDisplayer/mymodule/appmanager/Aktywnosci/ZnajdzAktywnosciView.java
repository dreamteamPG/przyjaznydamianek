package com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.dao.ActivityDao;
import com.przyjaznydamianek.dto.ActivityDto;
import com.przyjaznydamianek.models.Slide;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Czynnosci.ZarzadzanieCzynnosciamiView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivitySimpleAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.SlidesAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2014-12-25.
 */
public class ZnajdzAktywnosciView extends Activity implements AdapterView.OnItemClickListener {
    private ActivitySimpleAdapter listAdapter;
    private ListView mainListView;
    private MediaPlayer mp;
    private List<com.przyjaznydamianek.models.Activity> list;
    private ActivityDao activityDao;
    private Integer mode;

    @Override
    protected void onResume(){
        super.onResume();
    }

    public void znajdzClick(View v){
        String name="";
        EditText etName = (EditText) findViewById(R.id.editText);
        try {
            name = etName.getText().toString();
        } catch (Exception e){
        }

        Toast.makeText(v.getContext(), "Searching for: " + name, Toast.LENGTH_LONG).show();

        list = activityDao.getActivitiesByTitle(name);

        listAdapter = new ActivitySimpleAdapter(this, R.layout.rowlistlayoutactivitysimple,R.id.label, list);

        mainListView.setAdapter(listAdapter);

        mainListView.setOnItemClickListener((AdapterView.OnItemClickListener) this);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.znajdzaktywnosciview);
        try {
            mainListView = (ListView) findViewById(R.id.listView);
            activityDao = new ActivityDao(MySQLiteHelper.getDb());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        mode = (Integer)getIntent().getExtras().get("REQUESTCODE");
    }


    private class DialogInterfaceOnClickHelper implements DialogInterface.OnClickListener{
        private com.przyjaznydamianek.models.Activity activityToRemove;
        private Activity actvity;
        private List<String> ids;

        public void setActivityToRemoveId(com.przyjaznydamianek.models.Activity act, Activity actvity){
            this.activityToRemove = act;
            this.actvity = actvity;
        }
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    ActivityDto adto = new ActivityDto();
                    adto.setActivity(activityToRemove);
                    activityDao.delete(adto);
                    this.actvity.finish();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        com.przyjaznydamianek.models.Activity item = (com.przyjaznydamianek.models.Activity) mainListView.getAdapter().getItem(i);
        if(mode==RequestCodes.USUN_AKTYWNOSC){
            List <String> ids = activityDao.getPlanIdsOfSelectedActivity(item.getId());
            DialogInterfaceOnClickHelper dialogClickListener = new DialogInterfaceOnClickHelper();
            dialogClickListener.setActivityToRemoveId(item, this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Czy na pewno skasować aktywność \""+ item.getTitle() +"\" ? Dana aktywność używana jest w "+ ids.size() +" kontekstach i zostanie usunieta rowniez z nich. \n\n\n")
                    .setPositiveButton("Tak", dialogClickListener)
                    .setNegativeButton("Nie", dialogClickListener)
                    .show();
        } else{
            Intent intent = new Intent(this, AktwnosciAddEditView.class);
            intent.putExtra("ACTIVITY",item);
            if(mode==RequestCodes.EDYTUJ_ISTNIEJACA_AKTYWNOSC){
                startActivityForResult(intent, RequestCodes.ACTIVITY_EDITED);
            }
            if(mode==RequestCodes.PLAN_PRZERWA || mode==RequestCodes.PLAN_AKTYWNOSC || mode==RequestCodes.PLAN_GALERIA){
                setResult(mode, intent);
                super.finish();
            }
        }
    }

    public void playSound(View v) {
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                }
            }catch(Exception e){

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.finish();
    }
}