package com.przyjaznydamianekDisplayer.mymodule.appmanager.PlanyZarzadzanie;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.dao.PlanDao;
import com.przyjaznydamianek.dto.PlanDto;
import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianek.utils.BusinessLogic;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci.AktwnosciAddEditView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany.PlanyDodajAktywnoscView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany.PrzegladanieAktywnosci;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany.ZnajdzPlanView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivityAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

import java.util.ArrayList;

/**
 * Created by Michal on 2014-12-27.
 */
public class PrzydzielEdytujAktualnyPlan extends Activity implements AdapterView.OnItemClickListener {
    private Plan plan;
    private PlanDao planDao;
    private ActivityAdapter listAdapter;
    private ListView mainListView;
    private MediaPlayer mp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planyzarzadzanieprzydzieledytuj);
        try {
            planDao = new PlanDao(MySQLiteHelper.getDb());
            this.plan = planDao.getAktualnyPlan(BusinessLogic.SYSTEM_AKTUALNY_PLAN_ID);
        }catch (Exception e){
            System.out.println(e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            super.finish();
        }
        mainListView = (ListView) findViewById(R.id.listView);
        if(this.plan.getActivities()==null){
            this.plan.setActivities(new ArrayList<com.przyjaznydamianek.models.Activity>());
        }
        if(this.plan.getAktywnosciGaleriiAktywnosci()==null){
            this.plan.setAktywnosciGaleriiAktywnosci(new ArrayList<com.przyjaznydamianek.models.Activity>());
        }
        if(this.plan.getAktywnosciPrzerwy()==null){
            this.plan.setAktywnosciPrzerwy(new ArrayList<com.przyjaznydamianek.models.Activity>());
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        initList();
    }

    public void initList(){
        listAdapter = new ActivityAdapter(this, R.layout.rowlistlayout,R.id.label, this.plan.getActivities());
        mainListView.setAdapter(listAdapter);
        mainListView.setOnItemClickListener(this);
    }

    public void dodajAktywnoscClick(View v) {
        Intent intent = new Intent(this, PlanyDodajAktywnoscView.class);
        intent.putExtra("REQUESTCODE", RequestCodes.PLAN_DODAJ_AKTYWNOSC);
        startActivityForResult(intent,RequestCodes.PLAN_DODAJ_AKTYWNOSC);
    }

    public void edytujSlajd(View v){
        Intent intent = new Intent(this, AktwnosciAddEditView.class);
        int position = Integer.parseInt(v.getTag().toString());
        com.przyjaznydamianek.models.Activity aktywnosc = plan.getActivities().get(position);
        if(aktywnosc.getId().equals(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID)|| (aktywnosc.getTypeFlag()!=null && aktywnosc.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString()))){
            przegladajAktywnosciGaleriiClick(v);
        }else {
            intent.putExtra("ACTIVITY", aktywnosc);
            startActivityForResult(intent,RequestCodes.ACTIVITY_EDITED);
        }
    }

    public void usunSlajd(View v){
        int position = Integer.parseInt(v.getTag().toString());
        com.przyjaznydamianek.models.Activity aktywnosc = plan.getActivities().get(position);
        if(aktywnosc.getId().equals(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID) || (aktywnosc.getTypeFlag()!=null && aktywnosc.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString())) ){
            przegladajAktywnosciGaleriiClick(v);
        }else {
            listAdapter.remove(position);
        }
    }

    public void playSound(View v){
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }

    public void przegladajAktywnosciPrzerwyClick(View v) {
        Intent intent = new Intent(this, PrzegladanieAktywnosci.class);
        intent.putExtra("PLAN", this.plan);
        intent.putExtra("EDYCJAWYKONANIA", true);
        intent.putExtra("REQUESTCODE", RequestCodes.PRZEGLADANIE_PRZERWY);
        startActivityForResult(intent,RequestCodes.PRZEGLADANIE_PRZERWY);
    }

    public void przegladajAktywnosciGaleriiClick(View v) {
        Intent intent = new Intent(this, PrzegladanieAktywnosci.class);
        intent.putExtra("PLAN", this.plan);
        intent.putExtra("EDYCJAWYKONANIA", true);
        intent.putExtra("REQUESTCODE", RequestCodes.PRZEGLADANIE_GA);
        startActivityForResult(intent,RequestCodes.PRZEGLADANIE_GA);
    }

    private void refreshAllTheSameActivities(com.przyjaznydamianek.models.Activity editedActivity){
        for(int i = 0;i<this.plan.getActivities().size();i++){
            if(this.plan.getActivities().get(i).getId().equals(editedActivity.getId())){
                this.plan.getActivities().set(i,editedActivity);
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciGaleriiAktywnosci().size();i++){
            if(this.plan.getAktywnosciGaleriiAktywnosci().get(i).getId().equals(editedActivity.getId())){
                this.plan.getAktywnosciGaleriiAktywnosci().set(i,editedActivity);
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciPrzerwy().size();i++){
            if(this.plan.getAktywnosciPrzerwy().get(i).getId().equals(editedActivity.getId())){
                this.plan.getAktywnosciPrzerwy().set(i,editedActivity);
            }
        }
    }

    public void dodajAktywnosciZSzablonuClick(View v){
        Intent intent = new Intent(this, ZnajdzPlanView.class);
        intent.putExtra("REQUESTCODE",RequestCodes.DODAJ_AKTYWNOSCI_Z_SZABLONU);
        startActivityForResult(intent,RequestCodes.DODAJ_AKTYWNOSCI_Z_SZABLONU);
    }

    public void wyczyscPlanClick(View v){
        this.plan.setActivities(new ArrayList<com.przyjaznydamianek.models.Activity>());
        this.plan.setAktywnosciGaleriiAktywnosci(new ArrayList<com.przyjaznydamianek.models.Activity>());
        this.plan.setAktywnosciPrzerwy(new ArrayList<com.przyjaznydamianek.models.Activity>());
        initList();
    }

    private void dodajAktywnosciZSzablonu(Plan wybranyPlan){
        for(int i = 0;i<wybranyPlan.getActivities().size();i++){
            this.plan.getActivities().add(wybranyPlan.getActivities().get(i));
        }
        for(int i = 0;i<wybranyPlan.getAktywnosciGaleriiAktywnosci().size();i++){
            this.plan.getAktywnosciGaleriiAktywnosci().add(wybranyPlan.getAktywnosciGaleriiAktywnosci().get(i));
        }
        for(int i = 0;i<wybranyPlan.getAktywnosciPrzerwy().size();i++){
            this.plan.getAktywnosciPrzerwy().add(wybranyPlan.getAktywnosciPrzerwy().get(i));
        }
        String wiadomosc = "Dodano aktywnośći:\n";
        wiadomosc+=wybranyPlan.getActivities().size() + " Aktywności\n";
        wiadomosc+=wybranyPlan.getAktywnosciPrzerwy().size() + " Aktywności przerwy\n";
        wiadomosc+=wybranyPlan.getAktywnosciGaleriiAktywnosci().size() + " Aktywności typu GA";
        Toast.makeText(this, wiadomosc, Toast.LENGTH_LONG).show();
    }

    public void zapisz(View v){
        PlanDto pdto = new PlanDto();
        pdto.setPlan(this.plan);
        try{
            planDao.update((pdto));
            super.finish();
        } catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
    }

    public void wyczyscWykonanieCaloscioweClick(View v){
        com.przyjaznydamianek.models.Activity aktywnosc;
        for(int i = 0;i<this.plan.getActivities().size();i++){
            aktywnosc = this.plan.getActivities().get(i);
            if(aktywnosc.getStatus()!=null && aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                if(aktywnosc.getTypeFlag()!=null && aktywnosc.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString())){
                    aktywnosc = new com.przyjaznydamianek.models.Activity();
                    aktywnosc.setId(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID);
                    aktywnosc.setTitle("Galeria aktywności");
                    aktywnosc.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString());
                    this.plan.getActivities().set(i,aktywnosc);
                } else {
                    aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
                }
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciGaleriiAktywnosci().size();i++){
            aktywnosc = this.plan.getAktywnosciGaleriiAktywnosci().get(i);
            if(aktywnosc.getStatus()!=null && aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciPrzerwy().size();i++){
            aktywnosc = this.plan.getAktywnosciPrzerwy().get(i);
            if(aktywnosc.getStatus()!=null && aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
            }
        }
        listAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        com.przyjaznydamianek.models.Activity aktywnosc = null;
        if(data!=null && data.getExtras()!=null && data.getExtras().get("ACTIVITY")!=null){
            aktywnosc = (com.przyjaznydamianek.models.Activity)data.getExtras().get("ACTIVITY");
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_NOWA_AKTYWNOSC){
                Toast.makeText(this, "Dodano aktywność", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getActivities().add(aktywnosc);
                }
            }
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_AKTYWNOSC){
                Toast.makeText(this, "Dodano aktywność", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getActivities().add(aktywnosc);
                }
            }
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_PRZERWA){
                Toast.makeText(this, "Dodano aktywność przerwy", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getAktywnosciPrzerwy().add(aktywnosc);
                }
            }
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_GALERIA){
                Toast.makeText(this, "Dodano aktywność typu galeria aktywności", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getAktywnosciGaleriiAktywnosci().add(aktywnosc);
                    com.przyjaznydamianek.models.Activity gA = new com.przyjaznydamianek.models.Activity();
                    gA.setId(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID);
                    gA.setTitle("Galeria aktywności");
                    gA.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString());
                    this.plan.getActivities().add(gA);
                }
            }
            if(requestCode == RequestCodes.ACTIVITY_EDITED && resultCode ==  RequestCodes.ACTIVITY_EDITED){
                refreshAllTheSameActivities(aktywnosc);
                Toast.makeText(this, "Edytowano aktywnosc", Toast.LENGTH_LONG).show();
            }
        }
        if(data!=null && data.getExtras()!=null && data.getExtras().get("PLAN")!=null){
            Plan p = (Plan) data.getExtras().get("PLAN");
            if((requestCode == RequestCodes.PRZEGLADANIE_PRZERWY && resultCode ==  RequestCodes.PRZEGLADANIE_PRZERWY)
                    ||(requestCode == RequestCodes.PRZEGLADANIE_GA && resultCode ==  RequestCodes.PRZEGLADANIE_GA)){
                this.plan = p;
            }
            if(requestCode == RequestCodes.DODAJ_AKTYWNOSCI_Z_SZABLONU && resultCode ==  RequestCodes.DODAJ_AKTYWNOSCI_Z_SZABLONU){
                dodajAktywnosciZSzablonu(p);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        com.przyjaznydamianek.models.Activity aktywnosc = (com.przyjaznydamianek.models.Activity) mainListView.getAdapter().getItem(i);
        if(aktywnosc.getStatus()!=null && aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
            if(aktywnosc.getTypeFlag()!=null && aktywnosc.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.FINISHED_ACTIVITY_GALLERY.toString())){
                aktywnosc = new com.przyjaznydamianek.models.Activity();
                aktywnosc.setId(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID);
                aktywnosc.setTitle("Galeria aktywności");
                aktywnosc.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString());
                com.przyjaznydamianek.models.Activity aktywnosc2 = (com.przyjaznydamianek.models.Activity) mainListView.getAdapter().getItem(i);
                for(int j = 0;j<this.plan.getAktywnosciGaleriiAktywnosci().size();j++) {
                    if(this.plan.getAktywnosciGaleriiAktywnosci().get(j).getId().equals(aktywnosc2.getId()) && aktywnosc2.getStatus()!= null && aktywnosc2.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
                        aktywnosc2.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
                        this.plan.getAktywnosciGaleriiAktywnosci().set(j,aktywnosc2);
                        break;
                    }
                }
                this.plan.getActivities().set(i,aktywnosc);
            } else {
                aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
            }
        } else {
            if(aktywnosc.getStatus()==null || !aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())) {
                if (aktywnosc.getTypeFlag() != null && aktywnosc.getTypeFlag().equals(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString())) {
                    przegladajAktywnosciGaleriiClick(view);
                } else {
                    aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString());
                }
            }
        }
        listAdapter.notifyDataSetChanged();
    }
}
