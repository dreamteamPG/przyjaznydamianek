package com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

/**
 * Created by Michal on 2014-11-16.
 */
public class AktywnosciMainView extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aktywnoscimainview);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void dodajNowaAktywnosc(View v){
        Intent intent = new Intent(this, AktwnosciAddEditView.class);
        startActivityForResult(intent, RequestCodes.DODAJ_NOWA_AKTYWNOSC);
    }

    public void edytujIstniejacaAktywnosc(View v){
        Intent intent = new Intent(this, ZnajdzAktywnosciView.class);
        intent.putExtra("REQUESTCODE",RequestCodes.EDYTUJ_ISTNIEJACA_AKTYWNOSC);
        startActivityForResult(intent,RequestCodes.EDYTUJ_ISTNIEJACA_AKTYWNOSC);
    }

    public void usunAktywnosc(View v){
        Intent intent = new Intent(this, ZnajdzAktywnosciView.class);
        intent.putExtra("REQUESTCODE",RequestCodes.USUN_AKTYWNOSC);
        startActivityForResult(intent,RequestCodes.USUN_AKTYWNOSC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO
    }
}
