package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.przyjaznydamianek.DbHelper.MySQLiteHelper;
import com.przyjaznydamianek.dao.ActivityDao;
import com.przyjaznydamianek.dao.PlanDao;
import com.przyjaznydamianek.dto.PlanDto;
import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianek.models.Slide;
import com.przyjaznydamianek.sqlCreate.Aktywnosc;
import com.przyjaznydamianek.utils.BusinessLogic;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Aktywnosci.AktwnosciAddEditView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Czynnosci.CzynnosciAddEditView;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivityAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.SlidesAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2014-12-26.
 */
public class PlanyAddEditView extends Activity  {
    private int mode;

    private Plan plan;
    private PlanDao planDao;
    private ActivityAdapter listAdapter;
    private ListView mainListView;
    private MediaPlayer mp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planyaddeditview);
        if(getIntent().getExtras()!=null && getIntent().getExtras().get("PLAN")!=null){
            mode = RequestCodes.PLAN_EDITED;
            this.plan = (Plan)getIntent().getExtras().get("PLAN");
            EditText etName = (EditText) findViewById(R.id.editText);
            etName.setText(this.plan.getTitle());
        }
        else {
            mode = RequestCodes.PLAN_ADDED;
            this.plan = new Plan();
        }
        try {
            planDao = new PlanDao(MySQLiteHelper.getDb());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        mainListView = (ListView) findViewById(R.id.listView);
        if(this.plan.getActivities()==null){
            this.plan.setActivities(new ArrayList<com.przyjaznydamianek.models.Activity>());
        }
        if(this.plan.getAktywnosciGaleriiAktywnosci()==null){
            this.plan.setAktywnosciGaleriiAktywnosci(new ArrayList<com.przyjaznydamianek.models.Activity>());
        }
        if(this.plan.getAktywnosciPrzerwy()==null){
            this.plan.setAktywnosciPrzerwy(new ArrayList<com.przyjaznydamianek.models.Activity>());
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        initList();
    }

    public void initList(){
        listAdapter = new ActivityAdapter(this, R.layout.rowlistlayout,R.id.label, this.plan.getActivities());
        mainListView.setAdapter(listAdapter);
    }

    public void dodajAktywnoscClick(View v) {
        Intent intent = new Intent(this, PlanyDodajAktywnoscView.class);
        intent.putExtra("REQUESTCODE", RequestCodes.PLAN_DODAJ_AKTYWNOSC);
        startActivityForResult(intent,RequestCodes.PLAN_DODAJ_AKTYWNOSC);
    }

    public void edytujSlajd(View v){
        Intent intent = new Intent(this, AktwnosciAddEditView.class);
        int position = Integer.parseInt(v.getTag().toString());
        com.przyjaznydamianek.models.Activity aktywnosc = plan.getActivities().get(position);
        if(aktywnosc.getId().equals(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID)){
            przegladajAktywnosciGaleriiClick(v);
        }else {
            intent.putExtra("ACTIVITY", aktywnosc);
            startActivityForResult(intent,RequestCodes.ACTIVITY_EDITED);
        }
    }

    public void usunSlajd(View v){
        int position = Integer.parseInt(v.getTag().toString());
        com.przyjaznydamianek.models.Activity aktywnosc = plan.getActivities().get(position);
        if(aktywnosc.getId().equals(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID)){
            przegladajAktywnosciGaleriiClick(v);
        }else {
            listAdapter.remove(position);
        }
    }

    public void playSound(View v){
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }

    public void przegladajAktywnosciPrzerwyClick(View v) {
        Intent intent = new Intent(this, PrzegladanieAktywnosci.class);
        intent.putExtra("PLAN", this.plan);
        intent.putExtra("REQUESTCODE", RequestCodes.PRZEGLADANIE_PRZERWY);
        startActivityForResult(intent,RequestCodes.PRZEGLADANIE_PRZERWY);
    }

    public void przegladajAktywnosciGaleriiClick(View v) {
        Intent intent = new Intent(this, PrzegladanieAktywnosci.class);
        intent.putExtra("PLAN", this.plan);
        intent.putExtra("REQUESTCODE", RequestCodes.PRZEGLADANIE_GA);
        startActivityForResult(intent,RequestCodes.PRZEGLADANIE_GA);
    }

    private void refreshAllTheSameActivities(com.przyjaznydamianek.models.Activity editedActivity){
        for(int i = 0;i<this.plan.getActivities().size();i++){
            if(this.plan.getActivities().get(i).getId().equals(editedActivity.getId())){
                this.plan.getActivities().set(i,editedActivity);
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciGaleriiAktywnosci().size();i++){
            if(this.plan.getAktywnosciGaleriiAktywnosci().get(i).getId().equals(editedActivity.getId())){
                this.plan.getAktywnosciGaleriiAktywnosci().set(i,editedActivity);
            }
        }
        for(int i = 0;i<this.plan.getAktywnosciPrzerwy().size();i++){
            if(this.plan.getAktywnosciPrzerwy().get(i).getId().equals(editedActivity.getId())){
                this.plan.getAktywnosciPrzerwy().set(i,editedActivity);
            }
        }
    }

    public void zapisz(View v){
        EditText etName = (EditText) findViewById(R.id.editText);
        String name = etName.getText().toString();
        if(name==null || name==""){
            Toast.makeText(this, "Proszę ustaw nazwę", Toast.LENGTH_LONG).show();
            return;
        }
        this.plan.setTitle(name);
        PlanDto pdto = new PlanDto();
        pdto.setPlan(this.plan);
        try{
            if(mode == RequestCodes.PLAN_ADDED){
                planDao.create(pdto);
            } else {
                planDao.update((pdto));
            }
            super.finish();
        } catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
    }

    public void dodajWybraneZPlanuClick(View v) {
        Intent intent = new Intent(this, ZnajdzPlanView.class);
        intent.putExtra("REQUESTCODE",RequestCodes.DODAJ_WYBRANE_Z_PLANU_SZUKANIE);
        startActivityForResult(intent,RequestCodes.DODAJ_WYBRANE_Z_PLANU_SZUKANIE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        com.przyjaznydamianek.models.Activity aktywnosc = null;
        if(data!=null && data.getExtras()!=null && data.getExtras().get("ACTIVITY")!=null){
            aktywnosc = (com.przyjaznydamianek.models.Activity)data.getExtras().get("ACTIVITY");
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_NOWA_AKTYWNOSC){
                Toast.makeText(this, "Dodano aktywność", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getActivities().add(aktywnosc);
                }
            }
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_AKTYWNOSC){
                Toast.makeText(this, "Dodano aktywność", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getActivities().add(aktywnosc);
                }
            }
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_PRZERWA){
                Toast.makeText(this, "Dodano aktywność przerwy", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getAktywnosciPrzerwy().add(aktywnosc);
                }
            }
            if(requestCode == RequestCodes.PLAN_DODAJ_AKTYWNOSC && resultCode ==  RequestCodes.PLAN_GALERIA){
                Toast.makeText(this, "Dodano aktywność typu galeria aktywności", Toast.LENGTH_LONG).show();
                if(aktywnosc!=null){
                    this.plan.getAktywnosciGaleriiAktywnosci().add(aktywnosc);
                    com.przyjaznydamianek.models.Activity gA = new com.przyjaznydamianek.models.Activity();
                    gA.setId(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID);
                    gA.setTitle("Galeria aktywności");
                    gA.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString());
                    this.plan.getActivities().add(gA);
                }
            }
            if(requestCode == RequestCodes.ACTIVITY_EDITED && resultCode ==  RequestCodes.ACTIVITY_EDITED){
                refreshAllTheSameActivities(aktywnosc);
                Toast.makeText(this, "Edytowano aktywnosc", Toast.LENGTH_LONG).show();
            }
        }
        if(data!=null && data.getExtras()!=null && data.getExtras().get("PLAN")!=null){
            if((requestCode == RequestCodes.PRZEGLADANIE_PRZERWY && resultCode ==  RequestCodes.PRZEGLADANIE_PRZERWY)
                ||(requestCode == RequestCodes.PRZEGLADANIE_GA && resultCode ==  RequestCodes.PRZEGLADANIE_GA)){
                this.plan = (Plan) data.getExtras().get("PLAN");
            }
        }
        if(data!=null && data.getExtras()!=null && data.getExtras().get("ACTIVITIES")!=null && requestCode == RequestCodes.DODAJ_WYBRANE_Z_PLANU_SZUKANIE){
            for(com.przyjaznydamianek.models.Activity ac : (ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("ACTIVITIES")){
                this.plan.getActivities().add(ac);
            }
            for(com.przyjaznydamianek.models.Activity ac : (ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("BREAKACTIVITIES")){
                this.plan.getAktywnosciPrzerwy().add(ac);
            }
            for(com.przyjaznydamianek.models.Activity ac : (ArrayList<com.przyjaznydamianek.models.Activity>)data.getExtras().get("GAACTIVITIES")){
                this.plan.getAktywnosciGaleriiAktywnosci().add(ac);
                com.przyjaznydamianek.models.Activity gA = new com.przyjaznydamianek.models.Activity();
                gA.setId(BusinessLogic.SYSTEM_GALERIA_AKTYWNOSCI_ID);
                gA.setTitle("Galeria aktywności");
                gA.setTypeFlag(com.przyjaznydamianek.models.Activity.TypeFlag.TEMP_ACTIVITY_GALLERY.toString());
                this.plan.getActivities().add(gA);
            }
        }

    }
}
