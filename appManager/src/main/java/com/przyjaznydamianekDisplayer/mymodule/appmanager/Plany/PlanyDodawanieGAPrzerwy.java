package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.przyjaznydamianek.models.Plan;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.ActivitySimpleAdapter;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

import java.util.ArrayList;

/**
 * Created by Michal on 2015-04-23.
 */
public class PlanyDodawanieGAPrzerwy extends Activity implements AdapterView.OnItemClickListener {
    private ArrayList<com.przyjaznydamianek.models.Activity> wybrane;
    private ActivitySimpleAdapter listAdapter;
    private ListView mainListView;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plandodajwybranegaprzerwyview);
        try {
            this.wybrane = (ArrayList<com.przyjaznydamianek.models.Activity>)getIntent().getExtras().get("ACTIVITIES");
            mainListView = (ListView) findViewById(R.id.listView);
        }catch (Exception e){
            System.out.println(e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            super.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initList();
    }

    public void initList(){
        listAdapter = new ActivitySimpleAdapter(this, R.layout.rowlistlayoutactivitysimple,R.id.label, this.wybrane);
        mainListView.setAdapter(listAdapter);
        mainListView.setOnItemClickListener(this);
    }

    public void playSound(View v){
        String audioPath=(String)v.getTag();
        if(audioPath!=null && !audioPath.equals("")){
            try {
                if(this.mp==null||!this.mp.isPlaying()) {
                    this.mp = MediaPlayer.create(this, Uri.parse(audioPath));
                    mp.start();
                } else {
                    if(this.mp!=null&&this.mp.isPlaying()){
                        mp.stop();
                    }
                }
            }catch(Exception e){

            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        com.przyjaznydamianek.models.Activity aktywnosc = (com.przyjaznydamianek.models.Activity) mainListView.getAdapter().getItem(i);
        if(aktywnosc.getStatus()==null || !aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())) {
            aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString());
            listAdapter.notifyDataSetChanged();
        } else if (aktywnosc.getStatus().equals(com.przyjaznydamianek.models.Activity.ActivityStatus.FINISHED.toString())){
            aktywnosc.setStatus(com.przyjaznydamianek.models.Activity.ActivityStatus.NEW.toString());
            listAdapter.notifyDataSetChanged();
        }
    }

    public void zapisz (View v){
        Intent intent = new Intent();
        intent.putExtra("ACTIVITIES", wybrane);
        setResult(1, intent);
        super.finish();
    }
}
