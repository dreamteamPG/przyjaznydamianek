package com.przyjaznydamianekDisplayer.mymodule.appmanager.Plany;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.przyjaznydamianekDisplayer.mymodule.appmanager.R;
import com.przyjaznydamianekDisplayer.mymodule.appmanager.Utils.RequestCodes;

/**
 * Created by Michal on 2014-12-26.
 */
public class PlanyMainView  extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planmainview);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void nowyPlanClick(View v) {
        Intent intent = new Intent(this, PlanyAddEditView.class);
        startActivityForResult(intent, RequestCodes.PLAN_ADDED);
    }

    public void edycjaPlanuClick(View v) {
        Intent intent = new Intent(this, ZnajdzPlanView.class);
        intent.putExtra("REQUESTCODE",RequestCodes.EDYTUJ_PLAN);
        startActivityForResult(intent,RequestCodes.EDYTUJ_PLAN);
    }

    public void usunPlanClick(View v) {
        Intent intent = new Intent(this, ZnajdzPlanView.class);
        intent.putExtra("REQUESTCODE",RequestCodes.USUN_PLAN);
        startActivityForResult(intent,RequestCodes.USUN_PLAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO
    }
}