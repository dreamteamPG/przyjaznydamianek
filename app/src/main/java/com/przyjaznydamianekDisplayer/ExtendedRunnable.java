package com.przyjaznydamianekDisplayer;

import android.view.View;

/**
 * Created by chris on 03.01.15.
 */
public abstract class ExtendedRunnable implements Runnable {


    protected int time;

    public boolean isRunning = false;


    public ExtendedRunnable(){}



    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
