package com.przyjaznydamianekDisplayer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.przyjaznydamianek.*;
import com.example.przyjaznydamianek.R;

/**
 * Created by Michal on 2015-01-14.
 */
public class FinalView extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(com.example.przyjaznydamianek.R.layout.finalscreenview);
        ImageView image = (ImageView) findViewById(R.id.imageView);
        if(image!=null)
            ((View) (image.getParent())).setBackgroundColor(Color.WHITE);


    }
}
