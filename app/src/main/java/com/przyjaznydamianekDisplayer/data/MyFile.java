package com.przyjaznydamianekDisplayer.data;

import java.io.File;

/**
 * Created by Chris on 10/24/2014.
 */
public class MyFile extends File {

    private String dbname;

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public MyFile(String path, String DBNAME){
        super(path);
        dbname=new StringBuilder().append(path).append("/").append(DBNAME).toString();

    }



    public MyFile mkdirs2() {
        super.mkdirs();
        return this;
    }



}
