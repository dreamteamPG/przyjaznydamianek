package com.przyjaznydamianekDisplayer.data;

import java.io.Serializable;

@Deprecated
public class Slide implements Serializable{
	private long id;
    private long settingsId;
	private String text;
    private int status;
    private String imagePath;
    private String audioPath;
    private int time;

    private boolean defaultSettings;

    private SlideSettings settings;

    public SlideSettings getSettings() {

        return settings;
    }

    public void setSettings(SlideSettings settings) {
        this.settings = settings;
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String title) {
		this.text = title;
	}


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public int getTime() {
        return time;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setSettingsId(long id) {
        defaultSettings = id == 0 ? true : false;
        settingsId=id;
    }
    public long getSettingsId() {
        return settingsId;
    }

    public boolean isDefaultSettings() {
        return defaultSettings;
    }
}
