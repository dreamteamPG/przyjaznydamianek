package com.przyjaznydamianekDisplayer.Utils;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Chris on 7/24/2014.
 */
public abstract class StructureConverter {

    public final static  String SD_CARD_ROOT =  Environment.getExternalStorageDirectory().toString();
    protected  String rootFolder;
    protected  String rootFolderName;
    protected  boolean folderExist;

    protected ArrayList<File> files;

    public void open(String root){
        files = new ArrayList<File>();
        rootFolder = SD_CARD_ROOT+"/"+root;

        File directory = new File(rootFolder);
        folderExist = directory.exists();
        if(!folderExist) return;
        rootFolderName = directory.getName();
        File[] fList = directory.listFiles();

        for (File file : fList) {
            if (file.isFile()) {
                files.add(file);
            } else if (file.isDirectory()) {
                open(file.getAbsolutePath());
            }
        }
    }
}
