package com.przyjaznydamianekDisplayer;

import com.example.przyjaznydamianek.*;
import com.example.przyjaznydamianek.R;
import com.przyjaznydamianek.dao.ChoosenUserDao;
import com.przyjaznydamianek.models.TypyWidokuAktywnosci;
import com.przyjaznydamianek.models.TypyWidokuCzynnosci;
import com.przyjaznydamianek.models.TypyWidokuPlanuAktywnosci;
import com.przyjaznydamianek.models.User;
import com.przyjaznydamianek.models.UserPreferences;


/**
 * Created by chris on 02.01.15.
 */
public class Globals {

    public static final String DEFAULT_USER_NAME="default";

    public static User GetUser(){



        User user = new ChoosenUserDao ().getChoosenUser();
        if(user==null) return GenDefaultUser();
        else return user;
    }

    public static User GenDefaultUser(){

        User user = new User();

        UserPreferences preferences = new UserPreferences();

        preferences.setTypWidokuCzynnosci(TypyWidokuCzynnosci.basic);
        preferences.setTypyWidokuAktywnosci(TypyWidokuAktywnosci.big);
        preferences.setTypWidokuPlanuAtywnosci(TypyWidokuPlanuAktywnosci.list);
        preferences.setTimerSoundPath("");
        user.setId(DEFAULT_USER_NAME);
        user.setName(DEFAULT_USER_NAME);
        user.setPreferences(preferences);

        return user;

    }

    public static int getListTypeForPlanActivity(){
        User user = GetUser();
        TypyWidokuAktywnosci typ = user.getPreferences().getTypyWidokuAktywnosci();
        switch (typ) {
            case big:
                return R.layout.rowlistlayoutbig;
            case medium:
                return R.layout.rowlistlayout;
            case small:
                return R.layout.rowlistlayoutsmall;
            default:
                return R.layout.rowlistlayout;
        }
    }

    public static int getPlanActivityViewForUser(){

        User user = GetUser();
        TypyWidokuPlanuAktywnosci typ = user.getPreferences().getTypWidokuPlanuAtywnosci();
        TypyWidokuAktywnosci typAk = user.getPreferences().getTypyWidokuAktywnosci();

        if (typ.equals(TypyWidokuPlanuAktywnosci.list)) {
            return R.layout.planactivities;
        } else {
            switch (typAk) {
                case big:
                    return R.layout.planactivitiesbasicbig;
                case medium:
                    return R.layout.planactivitiesbasicview;
                case small:
                    return R.layout.planactivitiesbasicsmall;
                default:
                    return R.layout.planactivitiesbasicview;
            }
        }
    }

    public static int getNumberOfIconsInGridForUser() {
        User user = GetUser();
        TypyWidokuAktywnosci typ = user.getPreferences().getTypyWidokuAktywnosci();
        switch (typ) {
            case big:
                return 6;
            case medium:
                return 4;
            case small:
                return 2;
            default:
                return 4;
        }
    }

    public static  int getActivityViewForUser(){

        User user = GetUser();
        TypyWidokuAktywnosci typ = user.getPreferences().getTypyWidokuAktywnosci();

        if(user.getPreferences().getTypWidokuCzynnosci().equals(TypyWidokuCzynnosci.advanced)) {



            switch (typ) {
                case big:
                    return com.example.przyjaznydamianek.R.layout.advbigview;
                case medium:
                    return com.example.przyjaznydamianek.R.layout.advmediumview;
                case small:
                    return com.example.przyjaznydamianek.R.layout.advsmallview;
                default:
                    return com.example.przyjaznydamianek.R.layout.advmediumview;
            }
        }else{

            switch (typ) {
                case big:
                    return R.layout.basicbigview;
                case medium:
                    return R.layout.basicmediumview;
                case small:
                    return R.layout.basicsmallview;
                default:
                    return R.layout.basicmediumview;
            }


        }


    }
}
